Welcome to MultiCellDS, the MultiCellular Data Standard. This repository 
contains many files related to the standard. We have:

* The XML Schemas (XSDs) that define the standard
* The C++ API (auto generated using XSD/e)
* The Python API (auto generated using PyXB)
* The OWL files associated with MultiCellDS
* Scripts related to generating the OWL file from the XSDs
* Scripts related to generating CSV files from XML data structured according to the XSDs

The branches are:

* master (development)
* stable (minor bug fixes only)
* Beta_Pre_v1.0 (includes many more files made during the development of MultiCellDS).

Further README files can be found in the appropriate directories.

All code (except where noted) is licensed under the BSD 2 Clause license. All
XML, XSD, and XSLT files are CC BY 4.0 (except where noted).