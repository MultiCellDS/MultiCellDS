// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

namespace microenvironment
{
  // domain_sskel
  //

  inline
  void domain_sskel::
  name_serializer (::xml_schema::string_sskel& name)
  {
    this->name_serializer_ = &name;
  }

  inline
  void domain_sskel::
  variables_serializer (::variables::list_of_variables_sskel& s)
  {
    this->variables_serializer_ = &s;
  }

  inline
  void domain_sskel::
  variables_serializer (::xml_schema::serializer_map& m)
  {
    this->variables_serializer_map_ = &m;
  }

  inline
  void domain_sskel::
  experimental_condition_serializer (::variables::experimental_conditions_sskel& s)
  {
    this->experimental_condition_serializer_ = &s;
  }

  inline
  void domain_sskel::
  experimental_condition_serializer (::xml_schema::serializer_map& m)
  {
    this->experimental_condition_serializer_map_ = &m;
  }

  inline
  void domain_sskel::
  mesh_serializer (::mesh::mesh_sskel& s)
  {
    this->mesh_serializer_ = &s;
  }

  inline
  void domain_sskel::
  mesh_serializer (::xml_schema::serializer_map& m)
  {
    this->mesh_serializer_map_ = &m;
  }

  inline
  void domain_sskel::
  data_serializer (::variables::data_sskel& s)
  {
    this->data_serializer_ = &s;
  }

  inline
  void domain_sskel::
  data_serializer (::xml_schema::serializer_map& m)
  {
    this->data_serializer_map_ = &m;
  }

  inline
  void domain_sskel::
  custom_serializer (::common::custom_sskel& s)
  {
    this->custom_serializer_ = &s;
  }

  inline
  void domain_sskel::
  custom_serializer (::xml_schema::serializer_map& m)
  {
    this->custom_serializer_map_ = &m;
  }

  inline
  void domain_sskel::
  serializers (::xml_schema::string_sskel& name,
               ::variables::list_of_variables_sskel& variables,
               ::variables::experimental_conditions_sskel& experimental_condition,
               ::mesh::mesh_sskel& mesh,
               ::variables::data_sskel& data,
               ::common::custom_sskel& custom)
  {
    this->name_serializer_ = &name;
    this->variables_serializer_ = &variables;
    this->experimental_condition_serializer_ = &experimental_condition;
    this->mesh_serializer_ = &mesh;
    this->data_serializer_ = &data;
    this->custom_serializer_ = &custom;
  }

  inline
  void domain_sskel::
  serializer_maps (::xml_schema::serializer_map& variables,
                   ::xml_schema::serializer_map& experimental_condition,
                   ::xml_schema::serializer_map& mesh,
                   ::xml_schema::serializer_map& data,
                   ::xml_schema::serializer_map& custom)
  {
    this->variables_serializer_map_ = &variables;
    this->experimental_condition_serializer_map_ = &experimental_condition;
    this->mesh_serializer_map_ = &mesh;
    this->data_serializer_map_ = &data;
    this->custom_serializer_map_ = &custom;
  }

  inline
  domain_sskel::
  domain_sskel ()
  : domain_impl_ (0),
    name_serializer_ (0),
    variables_serializer_ (0),
    variables_serializer_map_ (0),
    experimental_condition_serializer_ (0),
    experimental_condition_serializer_map_ (0),
    mesh_serializer_ (0),
    mesh_serializer_map_ (0),
    data_serializer_ (0),
    data_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  inline
  domain_sskel::
  domain_sskel (domain_sskel* impl, void*)
  : ::xsde::cxx::serializer::non_validating::complex_content (impl, 0),
    domain_impl_ (impl),
    name_serializer_ (0),
    variables_serializer_ (0),
    variables_serializer_map_ (0),
    experimental_condition_serializer_ (0),
    experimental_condition_serializer_map_ (0),
    mesh_serializer_ (0),
    mesh_serializer_map_ (0),
    data_serializer_ (0),
    data_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  // microenvironment_sskel
  //

  inline
  void microenvironment_sskel::
  domain_serializer (::microenvironment::domain_sskel& s)
  {
    this->domain_serializer_ = &s;
  }

  inline
  void microenvironment_sskel::
  domain_serializer (::xml_schema::serializer_map& m)
  {
    this->domain_serializer_map_ = &m;
  }

  inline
  void microenvironment_sskel::
  vascular_network_serializer (::vascular::vascular_network_sskel& s)
  {
    this->vascular_network_serializer_ = &s;
  }

  inline
  void microenvironment_sskel::
  vascular_network_serializer (::xml_schema::serializer_map& m)
  {
    this->vascular_network_serializer_map_ = &m;
  }

  inline
  void microenvironment_sskel::
  basement_membrane_serializer (::basement::basement_membrane_sskel& s)
  {
    this->basement_membrane_serializer_ = &s;
  }

  inline
  void microenvironment_sskel::
  basement_membrane_serializer (::xml_schema::serializer_map& m)
  {
    this->basement_membrane_serializer_map_ = &m;
  }

  inline
  void microenvironment_sskel::
  custom_serializer (::common::custom_sskel& s)
  {
    this->custom_serializer_ = &s;
  }

  inline
  void microenvironment_sskel::
  custom_serializer (::xml_schema::serializer_map& m)
  {
    this->custom_serializer_map_ = &m;
  }

  inline
  void microenvironment_sskel::
  serializers (::microenvironment::domain_sskel& domain,
               ::vascular::vascular_network_sskel& vascular_network,
               ::basement::basement_membrane_sskel& basement_membrane,
               ::common::custom_sskel& custom)
  {
    this->domain_serializer_ = &domain;
    this->vascular_network_serializer_ = &vascular_network;
    this->basement_membrane_serializer_ = &basement_membrane;
    this->custom_serializer_ = &custom;
  }

  inline
  void microenvironment_sskel::
  serializer_maps (::xml_schema::serializer_map& domain,
                   ::xml_schema::serializer_map& vascular_network,
                   ::xml_schema::serializer_map& basement_membrane,
                   ::xml_schema::serializer_map& custom)
  {
    this->domain_serializer_map_ = &domain;
    this->vascular_network_serializer_map_ = &vascular_network;
    this->basement_membrane_serializer_map_ = &basement_membrane;
    this->custom_serializer_map_ = &custom;
  }

  inline
  microenvironment_sskel::
  microenvironment_sskel ()
  : microenvironment_impl_ (0),
    domain_serializer_ (0),
    domain_serializer_map_ (0),
    vascular_network_serializer_ (0),
    vascular_network_serializer_map_ (0),
    basement_membrane_serializer_ (0),
    basement_membrane_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  inline
  microenvironment_sskel::
  microenvironment_sskel (microenvironment_sskel* impl, void*)
  : ::xsde::cxx::serializer::non_validating::complex_content (impl, 0),
    microenvironment_impl_ (impl),
    domain_serializer_ (0),
    domain_serializer_map_ (0),
    vascular_network_serializer_ (0),
    vascular_network_serializer_map_ (0),
    basement_membrane_serializer_ (0),
    basement_membrane_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }
}

// Begin epilogue.
//
//
// End epilogue.

