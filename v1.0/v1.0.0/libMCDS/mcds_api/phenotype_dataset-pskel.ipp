// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

namespace phenotype_dataset
{
  // phenotype_dataset_pskel
  //

  inline
  void phenotype_dataset_pskel::
  keywords_parser (::xml_schema::string_pskel& p)
  {
    this->keywords_parser_ = &p;
  }

  inline
  void phenotype_dataset_pskel::
  ID_parser (::xml_schema::unsigned_long_pskel& p)
  {
    this->ID_parser_ = &p;
  }

  inline
  void phenotype_dataset_pskel::
  microenvironment_parser (::microenvironment::microenvironment_pskel& p)
  {
    this->microenvironment_parser_ = &p;
  }

  inline
  void phenotype_dataset_pskel::
  microenvironment_parser (::xml_schema::parser_map& m)
  {
    this->microenvironment_parser_map_ = &m;
  }

  inline
  void phenotype_dataset_pskel::
  phenotype_parser (::phenotype::phenotype_pskel& p)
  {
    this->phenotype_parser_ = &p;
  }

  inline
  void phenotype_dataset_pskel::
  phenotype_parser (::xml_schema::parser_map& m)
  {
    this->phenotype_parser_map_ = &m;
  }

  inline
  void phenotype_dataset_pskel::
  cell_part_parser (::phenotype_base::cell_parts_pskel& p)
  {
    this->cell_part_parser_ = &p;
  }

  inline
  void phenotype_dataset_pskel::
  cell_part_parser (::xml_schema::parser_map& m)
  {
    this->cell_part_parser_map_ = &m;
  }

  inline
  void phenotype_dataset_pskel::
  custom_parser (::common::custom_pskel& p)
  {
    this->custom_parser_ = &p;
  }

  inline
  void phenotype_dataset_pskel::
  custom_parser (::xml_schema::parser_map& m)
  {
    this->custom_parser_map_ = &m;
  }

  inline
  void phenotype_dataset_pskel::
  parsers (::xml_schema::string_pskel& keywords,
           ::xml_schema::unsigned_long_pskel& ID,
           ::microenvironment::microenvironment_pskel& microenvironment,
           ::phenotype::phenotype_pskel& phenotype,
           ::phenotype_base::cell_parts_pskel& cell_part,
           ::common::custom_pskel& custom)
  {
    this->keywords_parser_ = &keywords;
    this->ID_parser_ = &ID;
    this->microenvironment_parser_ = &microenvironment;
    this->phenotype_parser_ = &phenotype;
    this->cell_part_parser_ = &cell_part;
    this->custom_parser_ = &custom;
  }

  inline
  void phenotype_dataset_pskel::
  parser_maps (::xml_schema::parser_map& microenvironment,
               ::xml_schema::parser_map& phenotype,
               ::xml_schema::parser_map& cell_part,
               ::xml_schema::parser_map& custom)
  {
    this->microenvironment_parser_map_ = &microenvironment;
    this->phenotype_parser_map_ = &phenotype;
    this->cell_part_parser_map_ = &cell_part;
    this->custom_parser_map_ = &custom;
  }

  inline
  phenotype_dataset_pskel::
  phenotype_dataset_pskel ()
  : phenotype_dataset_impl_ (0),
    keywords_parser_ (0),
    ID_parser_ (0),
    microenvironment_parser_ (0),
    microenvironment_parser_map_ (0),
    phenotype_parser_ (0),
    phenotype_parser_map_ (0),
    cell_part_parser_ (0),
    cell_part_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  inline
  phenotype_dataset_pskel::
  phenotype_dataset_pskel (phenotype_dataset_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    phenotype_dataset_impl_ (impl),
    keywords_parser_ (0),
    ID_parser_ (0),
    microenvironment_parser_ (0),
    microenvironment_parser_map_ (0),
    phenotype_parser_ (0),
    phenotype_parser_map_ (0),
    cell_part_parser_ (0),
    cell_part_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }
}

// Begin epilogue.
//
//
// End epilogue.

