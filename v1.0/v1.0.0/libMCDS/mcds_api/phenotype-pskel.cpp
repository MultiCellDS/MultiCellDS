// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

#include "phenotype-pskel.hpp"

#include <assert.h>

#include <string.h>
#include <xsde/cxx/parser/substitution-map.hxx>
#include <xsde/cxx/parser/validating/inheritance-map.hxx>

static
const ::xsde::cxx::parser::substitution_map_init
_xsde_substitution_map_init_;

static
const ::xsde::cxx::parser::validating::inheritance_map_init
_xsde_inheritance_map_init_;

namespace phenotype
{
  // phenotype_elements_pskel
  //

  void phenotype_elements_pskel::
  adhesion (::phenotype_common::adhesion* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->adhesion (x);
  }

  void phenotype_elements_pskel::
  geometrical_properties (::phenotype_common::geometrical_properties* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->geometrical_properties (x);
  }

  void phenotype_elements_pskel::
  mass (::phenotype_common::mass* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->mass (x);
  }

  void phenotype_elements_pskel::
  mechanics (::phenotype_common::mechanics* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->mechanics (x);
  }

  void phenotype_elements_pskel::
  motility (::phenotype_common::motility* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->motility (x);
  }

  void phenotype_elements_pskel::
  PKPD (::pkpd::PKPD* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->PKPD (x);
  }

  void phenotype_elements_pskel::
  timescale (::phenotype_base::expected_timescale* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->timescale (x);
  }

  void phenotype_elements_pskel::
  transport_processes (::phenotype_common::transport_processes* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->transport_processes (x);
  }

  void phenotype_elements_pskel::
  custom (::common::custom* x)
  {
    if (this->phenotype_elements_impl_)
      this->phenotype_elements_impl_->custom (x);
  }

  void phenotype_elements_pskel::
  _reset ()
  {
    if (this->resetting_)
      return;

    typedef ::cell_cycle::cycles_and_deaths_pskel base;
    base::_reset ();

    this->v_state_stack_.clear ();

    this->resetting_ = true;

    if (this->adhesion_parser_)
      this->adhesion_parser_->_reset ();

    if (this->adhesion_parser_map_)
      this->adhesion_parser_map_->reset ();

    if (this->geometrical_properties_parser_)
      this->geometrical_properties_parser_->_reset ();

    if (this->geometrical_properties_parser_map_)
      this->geometrical_properties_parser_map_->reset ();

    if (this->mass_parser_)
      this->mass_parser_->_reset ();

    if (this->mass_parser_map_)
      this->mass_parser_map_->reset ();

    if (this->mechanics_parser_)
      this->mechanics_parser_->_reset ();

    if (this->mechanics_parser_map_)
      this->mechanics_parser_map_->reset ();

    if (this->motility_parser_)
      this->motility_parser_->_reset ();

    if (this->motility_parser_map_)
      this->motility_parser_map_->reset ();

    if (this->PKPD_parser_)
      this->PKPD_parser_->_reset ();

    if (this->PKPD_parser_map_)
      this->PKPD_parser_map_->reset ();

    if (this->timescale_parser_)
      this->timescale_parser_->_reset ();

    if (this->timescale_parser_map_)
      this->timescale_parser_map_->reset ();

    if (this->transport_processes_parser_)
      this->transport_processes_parser_->_reset ();

    if (this->transport_processes_parser_map_)
      this->transport_processes_parser_map_->reset ();

    if (this->custom_parser_)
      this->custom_parser_->_reset ();

    if (this->custom_parser_map_)
      this->custom_parser_map_->reset ();

    this->resetting_ = false;
  }

  const char* phenotype_elements_pskel::
  _static_type ()
  {
    return "phenotype_elements phenotype";
  }

  const char* phenotype_elements_pskel::
  _dynamic_type () const
  {
    return _static_type ();
  }

  static
  const ::xsde::cxx::parser::validating::inheritance_map_entry
  _xsde_phenotype_elements_pskel_inheritance_map_entry_ (
    phenotype_elements_pskel::_static_type (),
    ::cell_cycle::cycles_and_deaths_pskel::_static_type ());

  ::cell_cycle::cycles_and_deaths* phenotype_elements_pskel::
  post_cycles_and_deaths ()
  {
    assert (this->cycles_and_deaths_impl_);
    return this->cycles_and_deaths_impl_->post_cycles_and_deaths ();
  }

  // phenotype_pskel
  //

  void phenotype_pskel::
  type (const ::phenotype_base::phenotype_type& x)
  {
    if (this->phenotype_impl_)
      this->phenotype_impl_->type (x);
  }

  void phenotype_pskel::
  _reset ()
  {
    typedef ::phenotype::phenotype_elements_pskel base;
    base::_reset ();

    if (this->type_parser_)
      this->type_parser_->_reset ();
  }

  const char* phenotype_pskel::
  _static_type ()
  {
    return "phenotype phenotype";
  }

  const char* phenotype_pskel::
  _dynamic_type () const
  {
    return _static_type ();
  }

  static
  const ::xsde::cxx::parser::validating::inheritance_map_entry
  _xsde_phenotype_pskel_inheritance_map_entry_ (
    phenotype_pskel::_static_type (),
    ::phenotype::phenotype_elements_pskel::_static_type ());

  ::phenotype::phenotype_elements* phenotype_pskel::
  post_phenotype_elements ()
  {
    assert (this->phenotype_elements_impl_);
    return this->phenotype_elements_impl_->post_phenotype_elements ();
  }
}

#include <assert.h>

namespace phenotype
{
  // Element validation and dispatch functions for phenotype_elements_pskel.
  //
  bool phenotype_elements_pskel::
  _start_element_impl (const ::xsde::cxx::ro_string& ns,
                       const ::xsde::cxx::ro_string& n,
                       const char* t)
  {
    XSDE_UNUSED (t);

    ::xsde::cxx::parser::context& ctx = this->_context ();

    v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
    v_state_descr_* vd = vs.data + (vs.size - 1);

    if (vd->func == 0 && vd->state == 0)
    {
      typedef ::cell_cycle::cycles_and_deaths_pskel base;
      if (base::_start_element_impl (ns, n, t))
        return true;
      else
        vd->state = 1;
    }

    while (vd->func != 0)
    {
      (this->*vd->func) (vd->state, vd->count, ns, n, t, true);

      vd = vs.data + (vs.size - 1);

      if (vd->state == ~0UL && !ctx.error_type ())
        vd = vs.data + (--vs.size - 1);
      else
        break;
    }

    if (vd->func == 0)
    {
      if (vd->state != ~0UL)
      {
        unsigned long s = ~0UL;

        if ((n == "adhesion" && ns.empty ()) ||
            (n == "geometrical_properties" && ns.empty ()) ||
            (n == "mass" && ns.empty ()) ||
            (n == "mechanics" && ns.empty ()) ||
            (n == "motility" && ns.empty ()) ||
            (n == "PKPD" && ns.empty ()) ||
            (n == "timescale" && ns.empty ()) ||
            (n == "transport_processes" && ns.empty ()) ||
            (n == "custom" && ns.empty ()))
          s = 0UL;

        if (s != ~0UL)
        {
          vd->count++;
          vd->state = ~0UL;

          vd = vs.data + vs.size++;
          vd->func = &phenotype_elements_pskel::sequence_1;
          vd->state = s;
          vd->count = 0;

          this->sequence_1 (vd->state, vd->count, ns, n, t, true);
        }
        else
        {
          return false;
        }
      }
      else
        return false;
    }

    return true;
  }

  bool phenotype_elements_pskel::
  _end_element_impl (const ::xsde::cxx::ro_string& ns,
                     const ::xsde::cxx::ro_string& n)
  {
    v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
    v_state_descr_& vd = vs.data[vs.size - 1];

    if (vd.func == 0 && vd.state == 0)
    {
      typedef ::cell_cycle::cycles_and_deaths_pskel base;
      if (!base::_end_element_impl (ns, n))
        assert (false);
      return true;
    }

    assert (vd.func != 0);
    (this->*vd.func) (vd.state, vd.count, ns, n, 0, false);

    if (vd.state == ~0UL)
      vs.size--;

    return true;
  }

  void phenotype_elements_pskel::
  _pre_e_validate ()
  {
    this->v_state_stack_.push ();
    static_cast< v_state_* > (this->v_state_stack_.top ())->size = 0;

    v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
    v_state_descr_& vd = vs.data[vs.size++];

    vd.func = 0;
    vd.state = 0;
    vd.count = 0;
    typedef ::cell_cycle::cycles_and_deaths_pskel base;
    base::_pre_e_validate ();
  }

  void phenotype_elements_pskel::
  _post_e_validate ()
  {
    ::xsde::cxx::parser::context& ctx = this->_context ();

    typedef ::cell_cycle::cycles_and_deaths_pskel base;
    base::_post_e_validate ();

    if (ctx.error_type ())
      return;

    v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
    v_state_descr_* vd = vs.data + (vs.size - 1);

    ::xsde::cxx::ro_string empty;
    while (vd->func != 0)
    {
      (this->*vd->func) (vd->state, vd->count, empty, empty, 0, true);

      if (ctx.error_type ())
        return;

      assert (vd->state == ~0UL);
      vd = vs.data + (--vs.size - 1);
    }


    this->v_state_stack_.pop ();
  }

  void phenotype_elements_pskel::
  sequence_1 (unsigned long& state,
              unsigned long& count,
              const ::xsde::cxx::ro_string& ns,
              const ::xsde::cxx::ro_string& n,
              const char* t,
              bool start)
  {
    ::xsde::cxx::parser::context& ctx = this->_context ();

    XSDE_UNUSED (t);
    XSDE_UNUSED (ctx);

    switch (state)
    {
      case 0UL:
      {
        unsigned long s = ~0UL;

        if (n == "adhesion" && ns.empty ())
          s = 0UL;
        else if (n == "geometrical_properties" && ns.empty ())
          s = 1UL;
        else if (n == "mass" && ns.empty ())
          s = 2UL;
        else if (n == "mechanics" && ns.empty ())
          s = 3UL;
        else if (n == "motility" && ns.empty ())
          s = 4UL;
        else if (n == "PKPD" && ns.empty ())
          s = 5UL;
        else if (n == "timescale" && ns.empty ())
          s = 6UL;
        else if (n == "transport_processes" && ns.empty ())
          s = 7UL;
        else if (n == "custom" && ns.empty ())
          s = 8UL;

        if (s != ~0UL)
        {
          assert (start);
          count = 0;
          state = ~0UL;

          v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
          v_state_descr_& vd = vs.data[vs.size++];

          vd.func = &phenotype_elements_pskel::sequence_0;
          vd.state = s;
          vd.count = 0;

          this->sequence_0 (vd.state, vd.count, ns, n, t, true);
          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = ~0UL;
          // Fall through.
        }
      }
      case ~0UL:
        break;
    }
  }

  void phenotype_elements_pskel::
  sequence_0 (unsigned long& state,
              unsigned long& count,
              const ::xsde::cxx::ro_string& ns,
              const ::xsde::cxx::ro_string& n,
              const char* t,
              bool start)
  {
    ::xsde::cxx::parser::context& ctx = this->_context ();

    XSDE_UNUSED (t);
    XSDE_UNUSED (ctx);

    switch (state)
    {
      case 0UL:
      {
        if (n == "adhesion" && ns.empty ())
        {
          if (start)
          {
            ::phenotype_common::adhesion_pskel* p = 0;

            if (t == 0 && this->adhesion_parser_ != 0)
              p = this->adhesion_parser_;
            else
            {
              const char* ts = ::phenotype_common::adhesion_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->adhesion_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->adhesion_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->adhesion_parser_map_ != 0)
                  p = static_cast< ::phenotype_common::adhesion_pskel* > (
                    this->adhesion_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::phenotype_common::adhesion_pskel* p =
            static_cast< ::phenotype_common::adhesion_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::phenotype_common::adhesion* tmp = p->post_adhesion ();
              this->adhesion (tmp);
            }

            count = 0;
            state = 1UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 1UL;
          // Fall through.
        }
      }
      case 1UL:
      {
        if (n == "geometrical_properties" && ns.empty ())
        {
          if (start)
          {
            ::phenotype_common::geometrical_properties_pskel* p = 0;

            if (t == 0 && this->geometrical_properties_parser_ != 0)
              p = this->geometrical_properties_parser_;
            else
            {
              const char* ts = ::phenotype_common::geometrical_properties_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->geometrical_properties_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->geometrical_properties_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->geometrical_properties_parser_map_ != 0)
                  p = static_cast< ::phenotype_common::geometrical_properties_pskel* > (
                    this->geometrical_properties_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::phenotype_common::geometrical_properties_pskel* p =
            static_cast< ::phenotype_common::geometrical_properties_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::phenotype_common::geometrical_properties* tmp = p->post_geometrical_properties ();
              this->geometrical_properties (tmp);
            }

            count = 0;
            state = 2UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 2UL;
          // Fall through.
        }
      }
      case 2UL:
      {
        if (n == "mass" && ns.empty ())
        {
          if (start)
          {
            ::phenotype_common::mass_pskel* p = 0;

            if (t == 0 && this->mass_parser_ != 0)
              p = this->mass_parser_;
            else
            {
              const char* ts = ::phenotype_common::mass_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->mass_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->mass_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->mass_parser_map_ != 0)
                  p = static_cast< ::phenotype_common::mass_pskel* > (
                    this->mass_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::phenotype_common::mass_pskel* p =
            static_cast< ::phenotype_common::mass_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::phenotype_common::mass* tmp = p->post_mass ();
              this->mass (tmp);
            }

            count = 0;
            state = 3UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 3UL;
          // Fall through.
        }
      }
      case 3UL:
      {
        if (n == "mechanics" && ns.empty ())
        {
          if (start)
          {
            ::phenotype_common::mechanics_pskel* p = 0;

            if (t == 0 && this->mechanics_parser_ != 0)
              p = this->mechanics_parser_;
            else
            {
              const char* ts = ::phenotype_common::mechanics_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->mechanics_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->mechanics_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->mechanics_parser_map_ != 0)
                  p = static_cast< ::phenotype_common::mechanics_pskel* > (
                    this->mechanics_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::phenotype_common::mechanics_pskel* p =
            static_cast< ::phenotype_common::mechanics_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::phenotype_common::mechanics* tmp = p->post_mechanics ();
              this->mechanics (tmp);
            }

            count = 0;
            state = 4UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 4UL;
          // Fall through.
        }
      }
      case 4UL:
      {
        if (n == "motility" && ns.empty ())
        {
          if (start)
          {
            ::phenotype_common::motility_pskel* p = 0;

            if (t == 0 && this->motility_parser_ != 0)
              p = this->motility_parser_;
            else
            {
              const char* ts = ::phenotype_common::motility_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->motility_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->motility_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->motility_parser_map_ != 0)
                  p = static_cast< ::phenotype_common::motility_pskel* > (
                    this->motility_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::phenotype_common::motility_pskel* p =
            static_cast< ::phenotype_common::motility_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::phenotype_common::motility* tmp = p->post_motility ();
              this->motility (tmp);
            }

            count = 0;
            state = 5UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 5UL;
          // Fall through.
        }
      }
      case 5UL:
      {
        if (n == "PKPD" && ns.empty ())
        {
          if (start)
          {
            ::pkpd::PKPD_pskel* p = 0;

            if (t == 0 && this->PKPD_parser_ != 0)
              p = this->PKPD_parser_;
            else
            {
              const char* ts = ::pkpd::PKPD_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->PKPD_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->PKPD_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->PKPD_parser_map_ != 0)
                  p = static_cast< ::pkpd::PKPD_pskel* > (
                    this->PKPD_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::pkpd::PKPD_pskel* p =
            static_cast< ::pkpd::PKPD_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::pkpd::PKPD* tmp = p->post_PKPD ();
              this->PKPD (tmp);
            }

            count = 0;
            state = 6UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 6UL;
          // Fall through.
        }
      }
      case 6UL:
      {
        if (n == "timescale" && ns.empty ())
        {
          if (start)
          {
            ::phenotype_base::expected_timescale_pskel* p = 0;

            if (t == 0 && this->timescale_parser_ != 0)
              p = this->timescale_parser_;
            else
            {
              const char* ts = ::phenotype_base::expected_timescale_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->timescale_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->timescale_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->timescale_parser_map_ != 0)
                  p = static_cast< ::phenotype_base::expected_timescale_pskel* > (
                    this->timescale_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::phenotype_base::expected_timescale_pskel* p =
            static_cast< ::phenotype_base::expected_timescale_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::phenotype_base::expected_timescale* tmp = p->post_expected_timescale ();
              this->timescale (tmp);
            }

            count = 0;
            state = 7UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 7UL;
          // Fall through.
        }
      }
      case 7UL:
      {
        if (n == "transport_processes" && ns.empty ())
        {
          if (start)
          {
            ::phenotype_common::transport_processes_pskel* p = 0;

            if (t == 0 && this->transport_processes_parser_ != 0)
              p = this->transport_processes_parser_;
            else
            {
              const char* ts = ::phenotype_common::transport_processes_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->transport_processes_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->transport_processes_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->transport_processes_parser_map_ != 0)
                  p = static_cast< ::phenotype_common::transport_processes_pskel* > (
                    this->transport_processes_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::phenotype_common::transport_processes_pskel* p =
            static_cast< ::phenotype_common::transport_processes_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::phenotype_common::transport_processes* tmp = p->post_transport_processes ();
              this->transport_processes (tmp);
            }

            count = 0;
            state = 8UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 8UL;
          // Fall through.
        }
      }
      case 8UL:
      {
        if (n == "custom" && ns.empty ())
        {
          if (start)
          {
            ::common::custom_pskel* p = 0;

            if (t == 0 && this->custom_parser_ != 0)
              p = this->custom_parser_;
            else
            {
              const char* ts = ::common::custom_pskel::_static_type ();

              if (t == 0)
                t = ts;

              if (this->custom_parser_ != 0 && strcmp (t, ts) == 0)
                p = this->custom_parser_;
              else
              {
                if (t != ts &&
                    !::xsde::cxx::parser::validating::inheritance_map_instance ().check (t, ts))
                {
                  ctx.schema_error (::xsde::cxx::schema_error::not_derived);
                  return;
                }

                if (this->custom_parser_map_ != 0)
                  p = static_cast< ::common::custom_pskel* > (
                    this->custom_parser_map_->find (t));
              }
            }

            if (p)
            {
              p->pre ();
              ctx.nested_parser (p);
            }
          }
          else
          {
            ::common::custom_pskel* p =
            static_cast< ::common::custom_pskel* > (ctx.nested_parser ());

            if (p != 0)
            {
              ::common::custom* tmp = p->post_custom ();
              this->custom (tmp);
            }

            count = 0;
            state = ~0UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = ~0UL;
          // Fall through.
        }
      }
      case ~0UL:
        break;
    }
  }
}

namespace phenotype
{
  // Attribute validation and dispatch functions for phenotype_pskel.
  //
  bool phenotype_pskel::
  _attribute_impl_phase_one (const ::xsde::cxx::ro_string& ns,
                             const ::xsde::cxx::ro_string& n,
                             const ::xsde::cxx::ro_string& s)
  {
    ::xsde::cxx::parser::context& ctx = this->_context ();

    if (n == "type" && ns.empty ())
    {
      if (this->type_parser_)
      {
        this->type_parser_->pre ();

        this->type_parser_->_pre_impl (ctx);

        if (!ctx.error_type ())
          this->type_parser_->_characters (s);

        if (!ctx.error_type ())
          this->type_parser_->_post_impl ();

        if (!ctx.error_type ())
        {
          const ::phenotype_base::phenotype_type& tmp = this->type_parser_->post_phenotype_type ();

          this->type (tmp);
        }
      }

      return true;
    }

    typedef ::phenotype::phenotype_elements_pskel base;
    return base::_attribute_impl_phase_one (ns, n, s);
  }
}

namespace phenotype
{
}

// Begin epilogue.
//
//
// End epilogue.

