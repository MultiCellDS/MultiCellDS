// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#include <xsde/cxx/pre.hxx>

// Begin prologue.
//
//
// End prologue.

#include "variables.hpp"

#include <stdlib.h>
#include <new>

#include <xsde/cxx/guard.hxx>

namespace variables
{
  // amount_type
  //

  static const char* _xsde_amount_type_enumerators_[] = 
  {
    "concentration",
    "density",
    "volume_fraction",
    "volume_percent",
    "volume_percentage",
    "surface_density",
    "area_fraction",
    "area_percent",
    "area_percentage",
    "count",
    "partial_pressure",
    "surface"
  };

  const char* amount_type::
  string () const
  {
    return _xsde_amount_type_enumerators_[value_];
  }

  // variable
  //

  variable::
  variable ()
  {
    this->units_present_ = false;
    this->ID_present_ = false;
    this->type_present_ = false;
    this->ChEBI_ID_present_ = false;
    this->MeSH_ID_present_ = false;
    this->DrugBank_ID_present_ = false;
    this->GMO_ID_present_ = false;
    this->GO_ID_present_ = false;
    this->UniProt_ID_present_ = false;
    this->PR_ID_present_ = false;
    this->material_amount_ = 0;
    this->physical_parameter_set_ = 0;
  }

  variable::
  ~variable ()
  {
    delete this->material_amount_;
    delete this->physical_parameter_set_;
  }

  void variable::
  _copy (variable& c) const
  {
    XSDE_UNUSED (c);

    c.name (this->name ());

    if (this->units_present ())
      c.units (this->units ());

    if (this->ID_present ())
      c.ID (this->ID ());

    if (this->type_present ())
      c.type (this->type ());

    if (this->ChEBI_ID_present ())
      c.ChEBI_ID (this->ChEBI_ID ());

    if (this->MeSH_ID_present ())
      c.MeSH_ID (this->MeSH_ID ());

    if (this->DrugBank_ID_present ())
      c.DrugBank_ID (this->DrugBank_ID ());

    if (this->GMO_ID_present ())
      c.GMO_ID (this->GMO_ID ());

    if (this->GO_ID_present ())
      c.GO_ID (this->GO_ID ());

    if (this->UniProt_ID_present ())
      c.UniProt_ID (this->UniProt_ID ());

    if (this->PR_ID_present ())
      c.PR_ID (this->PR_ID ());

    if (this->material_amount_present ())
    {
      ::variables::material_amount* m = this->material_amount ()._clone ();
      c.material_amount (m);
    }

    if (this->physical_parameter_set_present ())
    {
      ::variables::physical_parameter_set* m = this->physical_parameter_set ()._clone ();
      c.physical_parameter_set (m);
    }
  }

  variable* variable::
  _clone () const
  {
    variable* c = new variable;
    ::xsde::cxx::guard< variable > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }

  // material_amount
  //

  material_amount::
  material_amount ()
  {
    this->type_present_ = false;
    this->scale_units_present_ = false;
  }

  material_amount::
  ~material_amount ()
  {
  }

  void material_amount::
  _copy (material_amount& c) const
  {
    XSDE_UNUSED (c);

    const ::common::units_decimal& b = *this;
    b._copy (c);
    if (this->type_present ())
      c.type (this->type ());

    if (this->scale_units_present ())
      c.scale_units (this->scale_units ());
  }

  material_amount* material_amount::
  _clone () const
  {
    material_amount* c = new material_amount;
    ::xsde::cxx::guard< material_amount > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }

  // physical_parameter_set
  //

  physical_parameter_set::
  physical_parameter_set ()
  {
    this->conditions_ = 0;
    this->diffusion_coefficient_ = 0;
    this->decay_rate_ = 0;
    this->custom_ = 0;
  }

  physical_parameter_set::
  ~physical_parameter_set ()
  {
    delete this->conditions_;
    delete this->diffusion_coefficient_;
    delete this->decay_rate_;
    delete this->custom_;
  }

  void physical_parameter_set::
  _copy (physical_parameter_set& c) const
  {
    XSDE_UNUSED (c);

    if (this->conditions_present ())
    {
      ::variables::physical_conditions* m = this->conditions ()._clone ();
      c.conditions (m);
    }

    if (this->diffusion_coefficient_present ())
    {
      ::common::units_decimal* m = this->diffusion_coefficient ()._clone ();
      c.diffusion_coefficient (m);
    }

    if (this->decay_rate_present ())
    {
      ::common::units_decimal* m = this->decay_rate ()._clone ();
      c.decay_rate (m);
    }

    if (this->custom_present ())
    {
      ::common::custom* m = this->custom ()._clone ();
      c.custom (m);
    }
  }

  physical_parameter_set* physical_parameter_set::
  _clone () const
  {
    physical_parameter_set* c = new physical_parameter_set;
    ::xsde::cxx::guard< physical_parameter_set > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }

  // physical_conditions
  //

  physical_conditions::
  physical_conditions ()
  {
    this->temperature_ = 0;
    this->mechanical_pressure_ = 0;
    this->acidity_ = 0;
    this->pH_ = 0;
    this->custom_ = 0;
  }

  physical_conditions::
  ~physical_conditions ()
  {
    delete this->temperature_;
    delete this->mechanical_pressure_;
    delete this->acidity_;
    delete this->pH_;
    delete this->custom_;
  }

  void physical_conditions::
  _copy (physical_conditions& c) const
  {
    XSDE_UNUSED (c);

    if (this->temperature_present ())
    {
      ::common::units_decimal* m = this->temperature ()._clone ();
      c.temperature (m);
    }

    if (this->mechanical_pressure_present ())
    {
      ::common::units_decimal* m = this->mechanical_pressure ()._clone ();
      c.mechanical_pressure (m);
    }

    if (this->acidity_present ())
    {
      ::common::units_decimal* m = this->acidity ()._clone ();
      c.acidity (m);
    }

    if (this->pH_present ())
    {
      ::common::units_decimal* m = this->pH ()._clone ();
      c.pH (m);
    }

    if (this->custom_present ())
    {
      ::common::custom* m = this->custom ()._clone ();
      c.custom (m);
    }
  }

  physical_conditions* physical_conditions::
  _clone () const
  {
    physical_conditions* c = new physical_conditions;
    ::xsde::cxx::guard< physical_conditions > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }

  // system
  //

  static const char* _xsde_system_enumerators_[] = 
  {
    "in vivo",
    "in vitro",
    "ex vivo",
    "in silico"
  };

  const char* system::
  string () const
  {
    return _xsde_system_enumerators_[value_];
  }

  // conditions
  //

  static const char* _xsde_conditions_enumerators_[] = 
  {
    "surface",
    "suspension",
    "spheroid"
  };

  const char* conditions::
  string () const
  {
    return _xsde_conditions_enumerators_[value_];
  }

  // experimental_conditions
  //

  experimental_conditions::
  experimental_conditions ()
  {
    this->type_present_ = false;
    this->dimensionality_present_ = false;
    this->system_present_ = false;
    this->conditions_present_ = false;
  }

  experimental_conditions::
  ~experimental_conditions ()
  {
  }

  void experimental_conditions::
  _copy (experimental_conditions& c) const
  {
    XSDE_UNUSED (c);

    if (this->type_present ())
      c.type (this->type ());

    if (this->dimensionality_present ())
      c.dimensionality (this->dimensionality ());

    if (this->system_present ())
      c.system (this->system ());

    if (this->conditions_present ())
      c.conditions (this->conditions ());

    this->surface_variable ().copy (c.surface_variable ());
  }

  experimental_conditions* experimental_conditions::
  _clone () const
  {
    experimental_conditions* c = new experimental_conditions;
    ::xsde::cxx::guard< experimental_conditions > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }

  // data_vector
  //

  data_vector::
  data_vector ()
  {
    this->voxel_ID_ = 0;
  }

  data_vector::
  ~data_vector ()
  {
    delete this->voxel_ID_;
  }

  void data_vector::
  _copy (data_vector& c) const
  {
    XSDE_UNUSED (c);

    const ::common::units_double_list& b = *this;
    b._copy (c);
    if (this->voxel_ID_present ())
    {
      ::common::unsigned_int_list* m = this->voxel_ID ()._clone ();
      c.voxel_ID (m);
    }
  }

  data_vector* data_vector::
  _clone () const
  {
    data_vector* c = new data_vector;
    ::xsde::cxx::guard< data_vector > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }

  // data
  //

  data::
  data ()
  {
    this->type_present_ = false;
    this->filename_present_ = false;
    this->custom_ = 0;
  }

  data::
  ~data ()
  {
    delete this->custom_;
  }

  void data::
  _copy (data& c) const
  {
    XSDE_UNUSED (c);

    if (this->type_present ())
      c.type (this->type ());

    if (this->filename_present ())
      c.filename (this->filename ());

    this->data_vector ().copy (c.data_vector ());

    if (this->custom_present ())
    {
      ::common::custom* m = this->custom ()._clone ();
      c.custom (m);
    }
  }

  data* data::
  _clone () const
  {
    data* c = new data;
    ::xsde::cxx::guard< data > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }

  // list_of_variables
  //

  list_of_variables::
  list_of_variables ()
  {
    this->physical_parameter_set_ = 0;
    this->custom_ = 0;
  }

  list_of_variables::
  ~list_of_variables ()
  {
    delete this->physical_parameter_set_;
    delete this->custom_;
  }

  void list_of_variables::
  _copy (list_of_variables& c) const
  {
    XSDE_UNUSED (c);

    this->variable ().copy (c.variable ());

    if (this->physical_parameter_set_present ())
    {
      ::variables::physical_parameter_set* m = this->physical_parameter_set ()._clone ();
      c.physical_parameter_set (m);
    }

    if (this->custom_present ())
    {
      ::common::custom* m = this->custom ()._clone ();
      c.custom (m);
    }
  }

  list_of_variables* list_of_variables::
  _clone () const
  {
    list_of_variables* c = new list_of_variables;
    ::xsde::cxx::guard< list_of_variables > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }

  // transition_threshold
  //

  transition_threshold::
  transition_threshold ()
  {
    this->ChEBI_ID_present_ = false;
    this->MeSH_ID_present_ = false;
    this->DrugBank_ID_present_ = false;
    this->GMO_ID_present_ = false;
    this->GO_ID_present_ = false;
    this->UniProt_ID_present_ = false;
    this->PR_ID_present_ = false;
  }

  transition_threshold::
  ~transition_threshold ()
  {
  }

  void transition_threshold::
  _copy (transition_threshold& c) const
  {
    XSDE_UNUSED (c);

    const ::common::transition_threshold& b = *this;
    b._copy (c);
    if (this->ChEBI_ID_present ())
      c.ChEBI_ID (this->ChEBI_ID ());

    if (this->MeSH_ID_present ())
      c.MeSH_ID (this->MeSH_ID ());

    if (this->DrugBank_ID_present ())
      c.DrugBank_ID (this->DrugBank_ID ());

    if (this->GMO_ID_present ())
      c.GMO_ID (this->GMO_ID ());

    if (this->GO_ID_present ())
      c.GO_ID (this->GO_ID ());

    if (this->UniProt_ID_present ())
      c.UniProt_ID (this->UniProt_ID ());

    if (this->PR_ID_present ())
      c.PR_ID (this->PR_ID ());
  }

  transition_threshold* transition_threshold::
  _clone () const
  {
    transition_threshold* c = new transition_threshold;
    ::xsde::cxx::guard< transition_threshold > g (c);
    this->_copy (*c);
    g.release ();
    return c;
  }
}

// Begin epilogue.
//
//
// End epilogue.

#include <xsde/cxx/post.hxx>

