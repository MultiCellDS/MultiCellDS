// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef METADATA_PIMPL_HPP
#define METADATA_PIMPL_HPP

#include <xsde/cxx/pre.hxx>

// Begin prologue.
//
//
// End prologue.

#ifndef XSDE_OMIT_PAGGR
#  define XSDE_OMIT_PAGGR
#  define METADATA_PIMPL_HPP_CLEAR_OMIT_PAGGR
#endif

#include "metadata-pskel.hpp"

#include <xsde/cxx/stack.hxx>

#include "common-pimpl.hpp"

namespace metadata
{
  class URL_pimpl: public URL_pskel
  {
    public:
    URL_pimpl ();

    virtual void
    pre ();

    virtual ::metadata::URL
    post_URL ();

    public:
    ::xml_schema::string_pimpl base_impl_;

    public:
    struct URL_pimpl_state
    {
      ::metadata::URL URL_;
    };

    URL_pimpl_state URL_pimpl_state_;
  };

  class orcid_identifier_pimpl: public orcid_identifier_pskel
  {
    public:
    virtual void
    pre ();

    // Elements.
    //
    virtual void
    path (const ::std::string&);

    virtual void
    given_names (const ::std::string&);

    virtual void
    family_name (const ::std::string&);

    virtual void
    email (const ::std::string&);

    virtual void
    url (const ::metadata::URL&);

    virtual void
    organization_name (const ::std::string&);

    virtual void
    department_name (const ::std::string&);

    virtual ::metadata::orcid_identifier
    post_orcid_identifier ();

    public:
    struct orcid_identifier_pimpl_state
    {
      ::metadata::orcid_identifier orcid_identifier_;
    };

    orcid_identifier_pimpl_state orcid_identifier_pimpl_state_;
  };

  class orcid_person_pimpl: public orcid_person_pskel
  {
    public:
    orcid_person_pimpl (bool = false);

    ~orcid_person_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    orcid_identifier (const ::metadata::orcid_identifier&);

    virtual ::metadata::orcid_person*
    post_orcid_person ();

    public:
    void
    pre_impl (::metadata::orcid_person*);

    public:
    struct orcid_person_pimpl_state
    {
      ::metadata::orcid_person* orcid_person_;
    };

    orcid_person_pimpl_state orcid_person_pimpl_state_;
    bool orcid_person_pimpl_base_;
  };

  class classification_pimpl: public classification_pskel
  {
    public:
    virtual void
    pre ();

    // Elements.
    //
    virtual void
    classification_number (const ::std::string&);

    virtual void
    line (unsigned int);

    virtual void
    variant (unsigned int);

    virtual void
    branch (unsigned int);

    virtual void
    version (unsigned int);

    virtual ::metadata::classification
    post_classification ();

    public:
    struct classification_pimpl_state
    {
      ::metadata::classification classification_;
    };

    classification_pimpl_state classification_pimpl_state_;
  };

  class curation_pimpl: public curation_pskel
  {
    public:
    curation_pimpl (bool = false);

    ~curation_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Attributes.
    //
    virtual void
    curated (bool);

    // Elements.
    //
    virtual void
    created (const ::xml_schema::date_time&);

    virtual void
    last_modified (const ::xml_schema::date_time&);

    virtual void
    choice_arm (choice_arm_tag);

    virtual void
    classification (const ::metadata::classification&);

    virtual void
    version (const ::std::string&);

    virtual void
    creator (::metadata::orcid_person*);

    virtual void
    current_contact (::metadata::orcid_person*);

    virtual void
    curator (::metadata::orcid_person*);

    virtual void
    last_modified_by (::metadata::orcid_person*);

    virtual ::metadata::curation*
    post_curation ();

    public:
    void
    pre_impl (::metadata::curation*);

    public:
    struct curation_pimpl_state
    {
      ::metadata::curation* curation_;
    };

    curation_pimpl_state curation_pimpl_state_;
    bool curation_pimpl_base_;
  };

  class citation_pimpl: public citation_pskel
  {
    public:
    citation_pimpl (bool = false);

    ~citation_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    text (const ::std::string&);

    virtual void
    DOI (const ::std::string&);

    virtual void
    URL (const ::metadata::URL&);

    virtual void
    PMID (const ::std::string&);

    virtual void
    PMCID (const ::std::string&);

    virtual void
    arXiv (const ::std::string&);

    virtual void
    notes (const ::std::string&);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::citation*
    post_citation ();

    public:
    void
    pre_impl (::metadata::citation*);

    public:
    struct citation_pimpl_state
    {
      ::metadata::citation* citation_;
    };

    citation_pimpl_state citation_pimpl_state_;
    bool citation_pimpl_base_;
  };

  class data_origin_pimpl: public data_origin_pskel
  {
    public:
    data_origin_pimpl (bool = false);

    ~data_origin_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Attributes.
    //
    virtual void
    ID (unsigned long long);

    // Elements.
    //
    virtual void
    instrumentation_information (const ::std::string&);

    virtual void
    experimental_protocol (const ::std::string&);

    virtual void
    citation (::metadata::citation*);

    virtual void
    xpath (const ::std::string&);

    virtual void
    notes (const ::std::string&);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::data_origin*
    post_data_origin ();

    public:
    void
    pre_impl (::metadata::data_origin*);

    public:
    struct data_origin_pimpl_state
    {
      ::metadata::data_origin* data_origin_;
    };

    data_origin_pimpl_state data_origin_pimpl_state_;
    bool data_origin_pimpl_base_;
  };

  class data_origins_pimpl: public data_origins_pskel
  {
    public:
    data_origins_pimpl (bool = false);

    ~data_origins_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    data_origin (::metadata::data_origin*);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::data_origins*
    post_data_origins ();

    public:
    void
    pre_impl (::metadata::data_origins*);

    public:
    struct data_origins_pimpl_state
    {
      ::metadata::data_origins* data_origins_;
    };

    data_origins_pimpl_state data_origins_pimpl_state_;
    bool data_origins_pimpl_base_;
  };

  class data_analysis_pimpl: public data_analysis_pskel
  {
    public:
    data_analysis_pimpl (bool = false);

    ~data_analysis_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    URL (const ::metadata::URL&);

    virtual void
    citation (::metadata::citation*);

    virtual void
    software (::metadata::software*);

    virtual void
    xpath (const ::std::string&);

    virtual void
    notes (const ::std::string&);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::data_analysis*
    post_data_analysis ();

    public:
    void
    pre_impl (::metadata::data_analysis*);

    public:
    struct data_analysis_pimpl_state
    {
      ::metadata::data_analysis* data_analysis_;
    };

    data_analysis_pimpl_state data_analysis_pimpl_state_;
    bool data_analysis_pimpl_base_;
  };

  class software_pimpl: public software_pskel
  {
    public:
    software_pimpl (bool = false);

    ~software_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    software_name (const ::std::string&);

    virtual void
    software_version (const ::std::string&);

    virtual void
    software_input_configuration (::common::custom*);

    virtual void
    name (const ::std::string&);

    virtual void
    version (const ::std::string&);

    virtual void
    input_configuration (::common::custom*);

    virtual void
    URL (const ::metadata::URL&);

    virtual void
    creator (::metadata::orcid_person*);

    virtual void
    citation (::metadata::citation*);

    virtual void
    user (::metadata::orcid_person*);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::software*
    post_software ();

    public:
    void
    pre_impl (::metadata::software*);

    public:
    struct software_pimpl_state
    {
      ::metadata::software* software_;
    };

    software_pimpl_state software_pimpl_state_;
    bool software_pimpl_base_;
  };

  class species_pimpl: public species_pskel
  {
    public:
    species_pimpl ();

    virtual void
    pre ();

    // Attributes.
    //
    virtual void
    MeSH_ID (const ::std::string&);

    virtual ::metadata::species
    post_species ();

    public:
    ::xml_schema::string_pimpl base_impl_;

    public:
    struct species_pimpl_state
    {
      ::metadata::species species_;
    };

    species_pimpl_state species_pimpl_state_;
  };

  class disease_pimpl: public disease_pskel
  {
    public:
    disease_pimpl ();

    virtual void
    pre ();

    // Attributes.
    //
    virtual void
    EFO_ID (const ::std::string&);

    virtual void
    DOID_ID (const ::std::string&);

    virtual ::metadata::disease
    post_disease ();

    public:
    ::xml_schema::string_pimpl base_impl_;

    public:
    struct disease_pimpl_state
    {
      ::metadata::disease disease_;
    };

    disease_pimpl_state disease_pimpl_state_;
  };

  class patient_derived_pimpl: public patient_derived_pskel
  {
    public:
    patient_derived_pimpl ();

    virtual void
    pre ();

    // Attributes.
    //
    virtual void
    patient_ID (const ::std::string&);

    virtual ::metadata::patient_derived
    post_patient_derived ();

    public:
    ::xml_schema::boolean_pimpl base_impl_;

    public:
    struct patient_derived_pimpl_state
    {
      ::metadata::patient_derived patient_derived_;
    };

    patient_derived_pimpl_state patient_derived_pimpl_state_;
  };

  class cell_origin_pimpl: public cell_origin_pskel
  {
    public:
    cell_origin_pimpl (bool = false);

    ~cell_origin_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    BTO_ID (const ::std::string&);

    virtual void
    CLO_ID (const ::std::string&);

    virtual void
    species (const ::metadata::species&);

    virtual void
    strain (const ::std::string&);

    virtual void
    organ (const ::std::string&);

    virtual void
    disease (const ::metadata::disease&);

    virtual void
    morphology (const ::std::string&);

    virtual void
    patient_derived (const ::metadata::patient_derived&);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::cell_origin*
    post_cell_origin ();

    public:
    void
    pre_impl (::metadata::cell_origin*);

    public:
    struct cell_origin_pimpl_state
    {
      ::metadata::cell_origin* cell_origin_;
    };

    cell_origin_pimpl_state cell_origin_pimpl_state_;
    bool cell_origin_pimpl_base_;
  };

  class MultiCellDB_pimpl: public MultiCellDB_pskel
  {
    public:
    virtual void
    pre ();

    // Elements.
    //
    virtual void
    ID (const ::std::string&);

    virtual void
    name (const ::std::string&);

    virtual ::metadata::MultiCellDB
    post_MultiCellDB ();

    public:
    struct MultiCellDB_pimpl_state
    {
      ::metadata::MultiCellDB MultiCellDB_;
    };

    MultiCellDB_pimpl_state MultiCellDB_pimpl_state_;
  };

  class rights_pimpl: public rights_pskel
  {
    public:
    rights_pimpl (bool = false);

    ~rights_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    license (::metadata::license*);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::rights*
    post_rights ();

    public:
    void
    pre_impl (::metadata::rights*);

    public:
    struct rights_pimpl_state
    {
      ::metadata::rights* rights_;
    };

    rights_pimpl_state rights_pimpl_state_;
    bool rights_pimpl_base_;
  };

  class license_pimpl: public license_pskel
  {
    public:
    license_pimpl (bool = false);

    ~license_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    LicenseDocument (::metadata::LicenseDocument*);

    virtual void
    notes (const ::std::string&);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::license*
    post_license ();

    public:
    void
    pre_impl (::metadata::license*);

    public:
    struct license_pimpl_state
    {
      ::metadata::license* license_;
    };

    license_pimpl_state license_pimpl_state_;
    bool license_pimpl_base_;
  };

  class LicenseDocument_pimpl: public LicenseDocument_pskel
  {
    public:
    LicenseDocument_pimpl (bool = false);

    ~LicenseDocument_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    name (const ::std::string&);

    virtual void
    URL (const ::metadata::URL&);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::LicenseDocument*
    post_LicenseDocument ();

    public:
    void
    pre_impl (::metadata::LicenseDocument*);

    public:
    struct LicenseDocument_pimpl_state
    {
      ::metadata::LicenseDocument* LicenseDocument_;
    };

    LicenseDocument_pimpl_state LicenseDocument_pimpl_state_;
    bool LicenseDocument_pimpl_base_;
  };

  class metadata_pimpl: public metadata_pskel
  {
    public:
    metadata_pimpl (bool = false);

    ~metadata_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    MultiCellDB (const ::metadata::MultiCellDB&);

    virtual void
    description (const ::std::string&);

    virtual void
    software (::metadata::software*);

    virtual void
    citation (::metadata::citation*);

    virtual void
    curation (::metadata::curation*);

    virtual void
    data_origins (::metadata::data_origins*);

    virtual void
    data_analysis (::metadata::data_analysis*);

    virtual void
    rights (::metadata::rights*);

    virtual void
    cell_origin (::metadata::cell_origin*);

    virtual void
    current_time (::common::units_decimal_nonnegative*);

    virtual void
    current_runtime (::common::units_decimal_nonnegative*);

    virtual void
    created (const ::xml_schema::date_time&);

    virtual void
    last_modified (const ::xml_schema::date_time&);

    virtual void
    notes (const ::std::string&);

    virtual void
    custom (::common::custom*);

    virtual ::metadata::metadata*
    post_metadata ();

    public:
    void
    pre_impl (::metadata::metadata*);

    public:
    struct metadata_pimpl_state
    {
      ::metadata::metadata* metadata_;
    };

    metadata_pimpl_state metadata_pimpl_state_;
    bool metadata_pimpl_base_;
  };
}

#ifdef METADATA_PIMPL_HPP_CLEAR_OMIT_PAGGR
#  undef XSDE_OMIT_PAGGR
#endif

#ifndef XSDE_OMIT_PAGGR

#endif // XSDE_OMIT_PAGGR

// Begin epilogue.
//
//
// End epilogue.

#include <xsde/cxx/post.hxx>

#endif // METADATA_PIMPL_HPP
