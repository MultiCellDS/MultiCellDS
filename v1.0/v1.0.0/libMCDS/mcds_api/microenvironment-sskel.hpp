// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef MICROENVIRONMENT_SSKEL_HPP
#define MICROENVIRONMENT_SSKEL_HPP

#include <xsde/cxx/version.hxx>

#if (XSDE_INT_VERSION != 3020000L)
#error XSD/e runtime version mismatch
#endif

#include <xsde/cxx/config.hxx>

#ifndef XSDE_ENCODING_UTF8
#error the generated code uses the UTF-8 encodingwhile the XSD/e runtime does not (reconfigure the runtime or change the --char-encoding value)
#endif

#ifndef XSDE_STL
#error the generated code uses STL while the XSD/e runtime does not (reconfigure the runtime or add --no-stl)
#endif

#ifndef XSDE_IOSTREAM
#error the generated code uses iostream while the XSD/e runtime does not (reconfigure the runtime or add --no-iostream)
#endif

#ifndef XSDE_EXCEPTIONS
#error the generated code uses exceptions while the XSD/e runtime does not (reconfigure the runtime or add --no-exceptions)
#endif

#ifndef XSDE_LONGLONG
#error the generated code uses long long while the XSD/e runtime does not (reconfigure the runtime or add --no-long-long)
#endif

#ifdef XSDE_SERIALIZER_VALIDATION
#error the XSD/e runtime uses validation while the generated code does not (reconfigure the runtime or remove --suppress-validation)
#endif

#ifndef XSDE_POLYMORPHIC
#error the generated code expects XSD/e runtime with polymorphism support (reconfigure the runtime or remove --generate-polymorphic/--runtime-polymorphic)
#endif

#ifndef XSDE_REUSE_STYLE_TIEIN
#error the generated code uses the tiein reuse style while the XSD/e runtime does not (reconfigure the runtime or add --reuse-style-mixin or --reuse-style-none)
#endif

#ifdef XSDE_CUSTOM_ALLOCATOR
#error the XSD/e runtime uses custom allocator while the generated code does not (reconfigure the runtime or add --custom-allocator)
#endif

#include <xsde/cxx/pre.hxx>

// Begin prologue.
//

#include <xsde/cxx/hybrid/any-type.hxx>
#include <xsde/cxx/hybrid/any-type-sskel.hxx>
#include <xsde/cxx/hybrid/any-type-simpl.hxx>

namespace xml_schema
{
  using ::xsde::cxx::hybrid::any_type;

  using ::xsde::cxx::hybrid::any_type_sskel;
  using ::xsde::cxx::hybrid::any_type_simpl;
}


//
// End prologue.

// Forward declarations
//
namespace microenvironment
{
  class domain_sskel;
  class microenvironment_sskel;
}


#include <string>

#include <xsde/cxx/serializer/xml-schema.hxx>

#include <xsde/cxx/serializer/exceptions.hxx>

#include <xsde/cxx/serializer/map.hxx>
#include <xsde/cxx/serializer/substitution-map-callback.hxx>
#include <xsde/cxx/serializer/substitution-map-load.hxx>

#include <xsde/cxx/serializer/non-validating/serializer.hxx>
#include <xsde/cxx/serializer/non-validating/xml-schema-sskel.hxx>
#include <xsde/cxx/serializer/non-validating/xml-schema-simpl.hxx>

#include <xsde/cxx/serializer/genx/document.hxx>

#include "microenvironment.hpp"

namespace xml_schema
{
  // Built-in XML Schema types mapping.
  //
  using ::xsde::cxx::string_sequence;
  using ::xsde::cxx::qname;
  using ::xsde::cxx::buffer;
  using ::xsde::cxx::time_zone;
  using ::xsde::cxx::gday;
  using ::xsde::cxx::gmonth;
  using ::xsde::cxx::gyear;
  using ::xsde::cxx::gmonth_day;
  using ::xsde::cxx::gyear_month;
  using ::xsde::cxx::date;
  using ::xsde::cxx::time;
  using ::xsde::cxx::date_time;
  using ::xsde::cxx::duration;

  // Base serializer skeletons.
  //
  using ::xsde::cxx::serializer::serializer_base;
  typedef ::xsde::cxx::serializer::non_validating::empty_content serializer_empty_content;
  typedef ::xsde::cxx::serializer::non_validating::simple_content serializer_simple_content;
  typedef ::xsde::cxx::serializer::non_validating::complex_content serializer_complex_content;

  // Serializer map interface and default implementation.
  //
  using ::xsde::cxx::serializer::serializer_map;
  using ::xsde::cxx::serializer::serializer_map_impl;

  // Serializer substitution map callack.
  //
  using ::xsde::cxx::serializer::serializer_smap_callback;

  // Substitution and inheritance hashmaps load querying.
  //
  using ::xsde::cxx::serializer::serializer_smap_buckets;
  using ::xsde::cxx::serializer::serializer_smap_elements;
  using ::xsde::cxx::serializer::serializer_smap_bucket_buckets;
  using ::xsde::cxx::serializer::serializer_smap_bucket_elements;

  // Serializer skeletons and implementations for the
  // XML Schema built-in types.
  //
  using ::xsde::cxx::serializer::non_validating::any_simple_type_sskel;
  using ::xsde::cxx::serializer::non_validating::any_simple_type_simpl;

  using ::xsde::cxx::serializer::non_validating::byte_sskel;
  using ::xsde::cxx::serializer::non_validating::byte_simpl;

  using ::xsde::cxx::serializer::non_validating::unsigned_byte_sskel;
  using ::xsde::cxx::serializer::non_validating::unsigned_byte_simpl;

  using ::xsde::cxx::serializer::non_validating::short_sskel;
  using ::xsde::cxx::serializer::non_validating::short_simpl;

  using ::xsde::cxx::serializer::non_validating::unsigned_short_sskel;
  using ::xsde::cxx::serializer::non_validating::unsigned_short_simpl;

  using ::xsde::cxx::serializer::non_validating::int_sskel;
  using ::xsde::cxx::serializer::non_validating::int_simpl;

  using ::xsde::cxx::serializer::non_validating::unsigned_int_sskel;
  using ::xsde::cxx::serializer::non_validating::unsigned_int_simpl;

  using ::xsde::cxx::serializer::non_validating::long_sskel;
  using ::xsde::cxx::serializer::non_validating::long_simpl;

  using ::xsde::cxx::serializer::non_validating::unsigned_long_sskel;
  using ::xsde::cxx::serializer::non_validating::unsigned_long_simpl;

  using ::xsde::cxx::serializer::non_validating::integer_sskel;
  using ::xsde::cxx::serializer::non_validating::integer_simpl;

  using ::xsde::cxx::serializer::non_validating::non_positive_integer_sskel;
  using ::xsde::cxx::serializer::non_validating::non_positive_integer_simpl;

  using ::xsde::cxx::serializer::non_validating::non_negative_integer_sskel;
  using ::xsde::cxx::serializer::non_validating::non_negative_integer_simpl;

  using ::xsde::cxx::serializer::non_validating::positive_integer_sskel;
  using ::xsde::cxx::serializer::non_validating::positive_integer_simpl;

  using ::xsde::cxx::serializer::non_validating::negative_integer_sskel;
  using ::xsde::cxx::serializer::non_validating::negative_integer_simpl;

  using ::xsde::cxx::serializer::non_validating::boolean_sskel;
  using ::xsde::cxx::serializer::non_validating::boolean_simpl;

  using ::xsde::cxx::serializer::non_validating::float_sskel;
  using ::xsde::cxx::serializer::non_validating::float_simpl;

  using ::xsde::cxx::serializer::non_validating::double_sskel;
  using ::xsde::cxx::serializer::non_validating::double_simpl;

  using ::xsde::cxx::serializer::non_validating::decimal_sskel;
  using ::xsde::cxx::serializer::non_validating::decimal_simpl;

  using ::xsde::cxx::serializer::non_validating::string_sskel;
  using ::xsde::cxx::serializer::non_validating::string_simpl;

  using ::xsde::cxx::serializer::non_validating::normalized_string_sskel;
  using ::xsde::cxx::serializer::non_validating::normalized_string_simpl;

  using ::xsde::cxx::serializer::non_validating::token_sskel;
  using ::xsde::cxx::serializer::non_validating::token_simpl;

  using ::xsde::cxx::serializer::non_validating::name_sskel;
  using ::xsde::cxx::serializer::non_validating::name_simpl;

  using ::xsde::cxx::serializer::non_validating::nmtoken_sskel;
  using ::xsde::cxx::serializer::non_validating::nmtoken_simpl;

  using ::xsde::cxx::serializer::non_validating::nmtokens_sskel;
  using ::xsde::cxx::serializer::non_validating::nmtokens_simpl;

  using ::xsde::cxx::serializer::non_validating::ncname_sskel;
  using ::xsde::cxx::serializer::non_validating::ncname_simpl;

  using ::xsde::cxx::serializer::non_validating::language_sskel;
  using ::xsde::cxx::serializer::non_validating::language_simpl;

  using ::xsde::cxx::serializer::non_validating::id_sskel;
  using ::xsde::cxx::serializer::non_validating::id_simpl;

  using ::xsde::cxx::serializer::non_validating::idref_sskel;
  using ::xsde::cxx::serializer::non_validating::idref_simpl;

  using ::xsde::cxx::serializer::non_validating::idrefs_sskel;
  using ::xsde::cxx::serializer::non_validating::idrefs_simpl;

  using ::xsde::cxx::serializer::non_validating::uri_sskel;
  using ::xsde::cxx::serializer::non_validating::uri_simpl;

  using ::xsde::cxx::serializer::non_validating::qname_sskel;
  using ::xsde::cxx::serializer::non_validating::qname_simpl;

  using ::xsde::cxx::serializer::non_validating::base64_binary_sskel;
  using ::xsde::cxx::serializer::non_validating::base64_binary_simpl;

  using ::xsde::cxx::serializer::non_validating::hex_binary_sskel;
  using ::xsde::cxx::serializer::non_validating::hex_binary_simpl;

  using ::xsde::cxx::serializer::non_validating::date_sskel;
  using ::xsde::cxx::serializer::non_validating::date_simpl;

  using ::xsde::cxx::serializer::non_validating::date_time_sskel;
  using ::xsde::cxx::serializer::non_validating::date_time_simpl;

  using ::xsde::cxx::serializer::non_validating::duration_sskel;
  using ::xsde::cxx::serializer::non_validating::duration_simpl;

  using ::xsde::cxx::serializer::non_validating::gday_sskel;
  using ::xsde::cxx::serializer::non_validating::gday_simpl;

  using ::xsde::cxx::serializer::non_validating::gmonth_sskel;
  using ::xsde::cxx::serializer::non_validating::gmonth_simpl;

  using ::xsde::cxx::serializer::non_validating::gmonth_day_sskel;
  using ::xsde::cxx::serializer::non_validating::gmonth_day_simpl;

  using ::xsde::cxx::serializer::non_validating::gyear_sskel;
  using ::xsde::cxx::serializer::non_validating::gyear_simpl;

  using ::xsde::cxx::serializer::non_validating::gyear_month_sskel;
  using ::xsde::cxx::serializer::non_validating::gyear_month_simpl;

  using ::xsde::cxx::serializer::non_validating::time_sskel;
  using ::xsde::cxx::serializer::non_validating::time_simpl;

  // Error codes.
  //
  typedef xsde::cxx::serializer::genx::xml_error serializer_xml_error;

  // Exceptions.
  //
  typedef xsde::cxx::serializer::exception serializer_exception;
  typedef xsde::cxx::serializer::xml serializer_xml;

  // Document serializer.
  //
  using xsde::cxx::serializer::genx::writer;
  using xsde::cxx::serializer::genx::document_simpl;

  // Serializer context.
  //
  typedef xsde::cxx::serializer::context serializer_context;
}

#include "common-sskel.hpp"

#include "mesh-sskel.hpp"

#include "basement_membrane-sskel.hpp"

#include "vascular-sskel.hpp"

#include "variables-sskel.hpp"

namespace microenvironment
{
  class domain_sskel: public ::xsde::cxx::serializer::non_validating::complex_content
  {
    public:
    // Serializer callbacks. Override them in your implementation.
    //

    virtual void
    pre (const ::microenvironment::domain&) = 0;

    // Attributes.
    //
    virtual bool
    name_present ();

    virtual ::std::string
    name () = 0;

    // Elements.
    //
    virtual bool
    variables_present ();

    virtual const ::variables::list_of_variables&
    variables () = 0;

    virtual bool
    experimental_condition_present ();

    virtual const ::variables::experimental_conditions&
    experimental_condition () = 0;

    virtual bool
    mesh_present ();

    virtual const ::mesh::mesh&
    mesh () = 0;

    virtual bool
    data_present ();

    virtual const ::variables::data&
    data () = 0;

    virtual bool
    custom_present ();

    virtual const ::common::custom&
    custom () = 0;

    // virtual void
    // post ();

    // Serializer construction API.
    //
    void
    serializers (::xml_schema::string_sskel& /* name */,
                 ::variables::list_of_variables_sskel& /* variables */,
                 ::variables::experimental_conditions_sskel& /* experimental_condition */,
                 ::mesh::mesh_sskel& /* mesh */,
                 ::variables::data_sskel& /* data */,
                 ::common::custom_sskel& /* custom */);

    void
    serializer_maps (::xml_schema::serializer_map& /* variables */,
                     ::xml_schema::serializer_map& /* experimental_condition */,
                     ::xml_schema::serializer_map& /* mesh */,
                     ::xml_schema::serializer_map& /* data */,
                     ::xml_schema::serializer_map& /* custom */);

    // Individual attribute serializers.
    //
    void
    name_serializer (::xml_schema::string_sskel&);

    // Individual element serializers.
    //
    void
    variables_serializer (::variables::list_of_variables_sskel&);

    void
    variables_serializer (::xml_schema::serializer_map&);

    void
    experimental_condition_serializer (::variables::experimental_conditions_sskel&);

    void
    experimental_condition_serializer (::xml_schema::serializer_map&);

    void
    mesh_serializer (::mesh::mesh_sskel&);

    void
    mesh_serializer (::xml_schema::serializer_map&);

    void
    data_serializer (::variables::data_sskel&);

    void
    data_serializer (::xml_schema::serializer_map&);

    void
    custom_serializer (::common::custom_sskel&);

    void
    custom_serializer (::xml_schema::serializer_map&);

    virtual void
    _reset ();

    // Constructor.
    //
    domain_sskel ();

    public:
    static const char*
    _static_type ();

    virtual const char*
    _dynamic_type () const;

    // Implementation.
    //
    public:
    virtual void
    _serialize_attributes ();

    virtual void
    _serialize_content ();

    protected:
    domain_sskel* domain_impl_;
    domain_sskel (domain_sskel*, void*);

    protected:
    ::xml_schema::string_sskel* name_serializer_;
    ::variables::list_of_variables_sskel* variables_serializer_;
    ::xml_schema::serializer_map* variables_serializer_map_;

    ::variables::experimental_conditions_sskel* experimental_condition_serializer_;
    ::xml_schema::serializer_map* experimental_condition_serializer_map_;

    ::mesh::mesh_sskel* mesh_serializer_;
    ::xml_schema::serializer_map* mesh_serializer_map_;

    ::variables::data_sskel* data_serializer_;
    ::xml_schema::serializer_map* data_serializer_map_;

    ::common::custom_sskel* custom_serializer_;
    ::xml_schema::serializer_map* custom_serializer_map_;
  };

  class microenvironment_sskel: public ::xsde::cxx::serializer::non_validating::complex_content
  {
    public:
    // Serializer callbacks. Override them in your implementation.
    //

    virtual void
    pre (const ::microenvironment::microenvironment&) = 0;

    // Elements.
    //
    virtual bool
    domain_next ();

    virtual const ::microenvironment::domain&
    domain () = 0;

    virtual bool
    vascular_network_next ();

    virtual const ::vascular::vascular_network&
    vascular_network () = 0;

    virtual bool
    basement_membrane_next ();

    virtual const ::basement::basement_membrane&
    basement_membrane () = 0;

    virtual bool
    custom_present ();

    virtual const ::common::custom&
    custom () = 0;

    // virtual void
    // post ();

    // Serializer construction API.
    //
    void
    serializers (::microenvironment::domain_sskel& /* domain */,
                 ::vascular::vascular_network_sskel& /* vascular_network */,
                 ::basement::basement_membrane_sskel& /* basement_membrane */,
                 ::common::custom_sskel& /* custom */);

    void
    serializer_maps (::xml_schema::serializer_map& /* domain */,
                     ::xml_schema::serializer_map& /* vascular_network */,
                     ::xml_schema::serializer_map& /* basement_membrane */,
                     ::xml_schema::serializer_map& /* custom */);

    // Individual element serializers.
    //
    void
    domain_serializer (::microenvironment::domain_sskel&);

    void
    domain_serializer (::xml_schema::serializer_map&);

    void
    vascular_network_serializer (::vascular::vascular_network_sskel&);

    void
    vascular_network_serializer (::xml_schema::serializer_map&);

    void
    basement_membrane_serializer (::basement::basement_membrane_sskel&);

    void
    basement_membrane_serializer (::xml_schema::serializer_map&);

    void
    custom_serializer (::common::custom_sskel&);

    void
    custom_serializer (::xml_schema::serializer_map&);

    virtual void
    _reset ();

    // Constructor.
    //
    microenvironment_sskel ();

    public:
    static const char*
    _static_type ();

    virtual const char*
    _dynamic_type () const;

    // Implementation.
    //
    public:
    virtual void
    _serialize_content ();

    protected:
    microenvironment_sskel* microenvironment_impl_;
    microenvironment_sskel (microenvironment_sskel*, void*);

    protected:
    ::microenvironment::domain_sskel* domain_serializer_;
    ::xml_schema::serializer_map* domain_serializer_map_;

    ::vascular::vascular_network_sskel* vascular_network_serializer_;
    ::xml_schema::serializer_map* vascular_network_serializer_map_;

    ::basement::basement_membrane_sskel* basement_membrane_serializer_;
    ::xml_schema::serializer_map* basement_membrane_serializer_map_;

    ::common::custom_sskel* custom_serializer_;
    ::xml_schema::serializer_map* custom_serializer_map_;
  };
}

#include "microenvironment-sskel.ipp"

// Begin epilogue.
//
//
// End epilogue.

#include <xsde/cxx/post.hxx>

#endif // MICROENVIRONMENT_SSKEL_HPP
