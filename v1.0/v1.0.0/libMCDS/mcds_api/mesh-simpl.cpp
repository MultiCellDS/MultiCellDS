// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

#include "mesh-simpl.hpp"

namespace mesh
{
  // bounding_box_simpl
  //

  bounding_box_simpl::
  bounding_box_simpl ()
  : bounding_box_sskel (&base_impl_)
  {
  }

  void bounding_box_simpl::
  pre (const ::mesh::bounding_box& x)
  {
    this->base_impl_.pre (x);
    this->bounding_box_simpl_state_.bounding_box_ = &x;
  }

  bool bounding_box_simpl::
  type_present ()
  {
    return this->bounding_box_simpl_state_.bounding_box_->type_present ();
  }

  ::std::string bounding_box_simpl::
  type ()
  {
    return this->bounding_box_simpl_state_.bounding_box_->type ();
  }

  // mesh_simpl
  //

  void mesh_simpl::
  pre (const ::mesh::mesh& x)
  {
    this->mesh_simpl_state_.mesh_ = &x;
  }

  bool mesh_simpl::
  type_present ()
  {
    return this->mesh_simpl_state_.mesh_->type_present ();
  }

  ::std::string mesh_simpl::
  type ()
  {
    return this->mesh_simpl_state_.mesh_->type ();
  }

  bool mesh_simpl::
  uniform_present ()
  {
    return this->mesh_simpl_state_.mesh_->uniform_present ();
  }

  bool mesh_simpl::
  uniform ()
  {
    return this->mesh_simpl_state_.mesh_->uniform ();
  }

  bool mesh_simpl::
  regular_present ()
  {
    return this->mesh_simpl_state_.mesh_->regular_present ();
  }

  bool mesh_simpl::
  regular ()
  {
    return this->mesh_simpl_state_.mesh_->regular ();
  }

  bool mesh_simpl::
  units_present ()
  {
    return this->mesh_simpl_state_.mesh_->units_present ();
  }

  ::std::string mesh_simpl::
  units ()
  {
    return this->mesh_simpl_state_.mesh_->units ();
  }

  bool mesh_simpl::
  bounding_box_present ()
  {
    return this->mesh_simpl_state_.mesh_->bounding_box_present ();
  }

  const ::mesh::bounding_box& mesh_simpl::
  bounding_box ()
  {
    return this->mesh_simpl_state_.mesh_->bounding_box ();
  }

  bool mesh_simpl::
  x_coordinates_present ()
  {
    return this->mesh_simpl_state_.mesh_->x_coordinates_present ();
  }

  const ::common::units_double_list& mesh_simpl::
  x_coordinates ()
  {
    return this->mesh_simpl_state_.mesh_->x_coordinates ();
  }

  bool mesh_simpl::
  y_coordinates_present ()
  {
    return this->mesh_simpl_state_.mesh_->y_coordinates_present ();
  }

  const ::common::units_double_list& mesh_simpl::
  y_coordinates ()
  {
    return this->mesh_simpl_state_.mesh_->y_coordinates ();
  }

  bool mesh_simpl::
  z_coordinates_present ()
  {
    return this->mesh_simpl_state_.mesh_->z_coordinates_present ();
  }

  const ::common::units_double_list& mesh_simpl::
  z_coordinates ()
  {
    return this->mesh_simpl_state_.mesh_->z_coordinates ();
  }

  bool mesh_simpl::
  voxels_present ()
  {
    return this->mesh_simpl_state_.mesh_->voxels_present ();
  }

  const ::mesh::list_of_voxels& mesh_simpl::
  voxels ()
  {
    return this->mesh_simpl_state_.mesh_->voxels ();
  }

  bool mesh_simpl::
  custom_present ()
  {
    return this->mesh_simpl_state_.mesh_->custom_present ();
  }

  const ::common::custom& mesh_simpl::
  custom ()
  {
    return this->mesh_simpl_state_.mesh_->custom ();
  }

  // list_of_voxels_simpl
  //

  void list_of_voxels_simpl::
  pre (const ::mesh::list_of_voxels& x)
  {
    this->list_of_voxels_simpl_state_.list_of_voxels_ = &x;
    this->list_of_voxels_simpl_state_.voxel_ = 
    this->list_of_voxels_simpl_state_.list_of_voxels_->voxel ().begin ();
    this->list_of_voxels_simpl_state_.voxel_end_ = 
    this->list_of_voxels_simpl_state_.list_of_voxels_->voxel ().end ();
  }

  bool list_of_voxels_simpl::
  type_present ()
  {
    return this->list_of_voxels_simpl_state_.list_of_voxels_->type_present ();
  }

  const ::common::data_storage_formats& list_of_voxels_simpl::
  type ()
  {
    return this->list_of_voxels_simpl_state_.list_of_voxels_->type ();
  }

  bool list_of_voxels_simpl::
  filename_present ()
  {
    return this->list_of_voxels_simpl_state_.list_of_voxels_->filename_present ();
  }

  ::std::string list_of_voxels_simpl::
  filename ()
  {
    return this->list_of_voxels_simpl_state_.list_of_voxels_->filename ();
  }

  bool list_of_voxels_simpl::
  voxel_next ()
  {
    return this->list_of_voxels_simpl_state_.voxel_ != 
    this->list_of_voxels_simpl_state_.voxel_end_;
  }

  const ::mesh::voxel& list_of_voxels_simpl::
  voxel ()
  {
    return *this->list_of_voxels_simpl_state_.voxel_++;
  }

  bool list_of_voxels_simpl::
  custom_present ()
  {
    return this->list_of_voxels_simpl_state_.list_of_voxels_->custom_present ();
  }

  const ::common::custom& list_of_voxels_simpl::
  custom ()
  {
    return this->list_of_voxels_simpl_state_.list_of_voxels_->custom ();
  }

  // voxel_simpl
  //

  void voxel_simpl::
  pre (const ::mesh::voxel& x)
  {
    this->voxel_simpl_state_.voxel_ = &x;
  }

  bool voxel_simpl::
  ID_present ()
  {
    return this->voxel_simpl_state_.voxel_->ID_present ();
  }

  unsigned int voxel_simpl::
  ID ()
  {
    return this->voxel_simpl_state_.voxel_->ID ();
  }

  bool voxel_simpl::
  type_present ()
  {
    return this->voxel_simpl_state_.voxel_->type_present ();
  }

  ::std::string voxel_simpl::
  type ()
  {
    return this->voxel_simpl_state_.voxel_->type ();
  }

  const ::common::units_double_list& voxel_simpl::
  center ()
  {
    return this->voxel_simpl_state_.voxel_->center ();
  }

  const ::common::units_decimal_nonnegative& voxel_simpl::
  volume ()
  {
    return this->voxel_simpl_state_.voxel_->volume ();
  }

  bool voxel_simpl::
  custom_present ()
  {
    return this->voxel_simpl_state_.voxel_->custom_present ();
  }

  const ::common::custom& voxel_simpl::
  custom ()
  {
    return this->voxel_simpl_state_.voxel_->custom ();
  }

  // node_simpl
  //

  void node_simpl::
  pre (const ::mesh::node& x)
  {
    this->node_simpl_state_.node_ = &x;
  }

  bool node_simpl::
  ID_present ()
  {
    return this->node_simpl_state_.node_->ID_present ();
  }

  unsigned int node_simpl::
  ID ()
  {
    return this->node_simpl_state_.node_->ID ();
  }

  const ::common::units_double_list& node_simpl::
  position ()
  {
    return this->node_simpl_state_.node_->position ();
  }

  bool node_simpl::
  custom_present ()
  {
    return this->node_simpl_state_.node_->custom_present ();
  }

  const ::common::custom& node_simpl::
  custom ()
  {
    return this->node_simpl_state_.node_->custom ();
  }

  // edge_simpl
  //

  void edge_simpl::
  pre (const ::mesh::edge& x)
  {
    this->edge_simpl_state_.edge_ = &x;
    this->edge_simpl_state_.node_ID_ = 
    this->edge_simpl_state_.edge_->node_ID ().begin ();
    this->edge_simpl_state_.node_ID_end_ = 
    this->edge_simpl_state_.edge_->node_ID ().end ();
  }

  bool edge_simpl::
  ID_present ()
  {
    return this->edge_simpl_state_.edge_->ID_present ();
  }

  unsigned int edge_simpl::
  ID ()
  {
    return this->edge_simpl_state_.edge_->ID ();
  }

  bool edge_simpl::
  node_ID_next ()
  {
    return this->edge_simpl_state_.node_ID_ != 
    this->edge_simpl_state_.node_ID_end_;
  }

  unsigned int edge_simpl::
  node_ID ()
  {
    return *this->edge_simpl_state_.node_ID_++;
  }

  // face_simpl
  //

  void face_simpl::
  pre (const ::mesh::face& x)
  {
    this->face_simpl_state_.face_ = &x;
    this->face_simpl_state_.edge_ID_ = 
    this->face_simpl_state_.face_->edge_ID ().begin ();
    this->face_simpl_state_.edge_ID_end_ = 
    this->face_simpl_state_.face_->edge_ID ().end ();
  }

  bool face_simpl::
  ID_present ()
  {
    return this->face_simpl_state_.face_->ID_present ();
  }

  unsigned int face_simpl::
  ID ()
  {
    return this->face_simpl_state_.face_->ID ();
  }

  bool face_simpl::
  edge_ID_next ()
  {
    return this->face_simpl_state_.edge_ID_ != 
    this->face_simpl_state_.edge_ID_end_;
  }

  unsigned int face_simpl::
  edge_ID ()
  {
    return *this->face_simpl_state_.edge_ID_++;
  }

  // int_list_xpath_simpl
  //

  int_list_xpath_simpl::
  int_list_xpath_simpl ()
  : int_list_xpath_sskel (&base_impl_)
  {
  }

  void int_list_xpath_simpl::
  pre (const ::mesh::int_list_xpath& x)
  {
    this->base_impl_.pre (x);
    this->int_list_xpath_simpl_state_.int_list_xpath_ = &x;
  }

  bool int_list_xpath_simpl::
  xpath_present ()
  {
    return this->int_list_xpath_simpl_state_.int_list_xpath_->xpath_present ();
  }

  ::std::string int_list_xpath_simpl::
  xpath ()
  {
    return this->int_list_xpath_simpl_state_.int_list_xpath_->xpath ();
  }

  bool int_list_xpath_simpl::
  grouping_number_present ()
  {
    return this->int_list_xpath_simpl_state_.int_list_xpath_->grouping_number_present ();
  }

  unsigned short int_list_xpath_simpl::
  grouping_number ()
  {
    return this->int_list_xpath_simpl_state_.int_list_xpath_->grouping_number ();
  }
}

// Begin epilogue.
//
//
// End epilogue.

