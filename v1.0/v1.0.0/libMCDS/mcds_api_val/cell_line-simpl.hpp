// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef CELL_LINE_SIMPL_HPP
#define CELL_LINE_SIMPL_HPP

#include <xsde/cxx/pre.hxx>

// Begin prologue.
//
//
// End prologue.

#ifndef XSDE_OMIT_SAGGR
#  define XSDE_OMIT_SAGGR
#  define CELL_LINE_SIMPL_HPP_CLEAR_OMIT_SAGGR
#endif

#include "cell_line-sskel.hpp"

#include <xsde/cxx/stack.hxx>

#include "metadata-simpl.hpp"

#include "phenotype_dataset-simpl.hpp"

#include "common-simpl.hpp"

namespace cell_line
{
  class cell_line_simpl: public cell_line_sskel
  {
    public:
    virtual void
    pre (const ::cell_line::cell_line&);

    // Attributes.
    //
    virtual bool
    ID_present ();

    virtual ::std::string
    ID ();

    virtual bool
    label_present ();

    virtual ::std::string
    label ();

    virtual bool
    curated_present ();

    virtual bool
    curated ();

    // Elements.
    //
    virtual bool
    metadata_present ();

    virtual const ::metadata::metadata&
    metadata ();

    virtual bool
    phenotype_dataset_next ();

    virtual const ::phenotype_dataset::phenotype_dataset&
    phenotype_dataset ();

    virtual bool
    custom_present ();

    virtual const ::common::custom&
    custom ();

    public:
    struct cell_line_simpl_state
    {
      const ::cell_line::cell_line* cell_line_;
      ::cell_line::cell_line::phenotype_dataset_const_iterator phenotype_dataset_;
      ::cell_line::cell_line::phenotype_dataset_const_iterator phenotype_dataset_end_;
    };

    cell_line_simpl_state cell_line_simpl_state_;
  };

  class DCLs_simpl: public DCLs_sskel
  {
    public:
    virtual void
    pre (const ::cell_line::DCLs&);

    // Elements.
    //
    virtual bool
    cell_line_next ();

    virtual const ::cell_line::cell_line&
    cell_line ();

    public:
    struct DCLs_simpl_state
    {
      const ::cell_line::DCLs* DCLs_;
      ::cell_line::DCLs::cell_line_const_iterator cell_line_;
      ::cell_line::DCLs::cell_line_const_iterator cell_line_end_;
    };

    DCLs_simpl_state DCLs_simpl_state_;
  };
}

#ifdef CELL_LINE_SIMPL_HPP_CLEAR_OMIT_SAGGR
#  undef XSDE_OMIT_SAGGR
#endif

#ifndef XSDE_OMIT_SAGGR

#endif // XSDE_OMIT_SAGGR

// Begin epilogue.
//
//
// End epilogue.

#include <xsde/cxx/post.hxx>

#endif // CELL_LINE_SIMPL_HPP
