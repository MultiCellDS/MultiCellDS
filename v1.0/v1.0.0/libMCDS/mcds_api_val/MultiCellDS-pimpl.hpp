// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef MULTI_CELL_DS_PIMPL_HPP
#define MULTI_CELL_DS_PIMPL_HPP

#include <xsde/cxx/pre.hxx>

// Begin prologue.
//
//
// End prologue.

#ifndef XSDE_OMIT_PAGGR
#  define XSDE_OMIT_PAGGR
#  define MULTI_CELL_DS_PIMPL_HPP_CLEAR_OMIT_PAGGR
#endif

#include "MultiCellDS-pskel.hpp"

#include <xsde/cxx/stack.hxx>

#include "common-pimpl.hpp"

#include "metadata-pimpl.hpp"

#include "cell_line-pimpl.hpp"

#include "cell-pimpl.hpp"

#include "microenvironment-pimpl.hpp"

class MCDS_type_pimpl: public MCDS_type_pskel
{
  public:
  MCDS_type_pimpl ();

  virtual void
  pre ();

  virtual void
  _characters (const ::xsde::cxx::ro_string&);

  virtual void
  _post ();

  virtual ::MCDS_type
  post_MCDS_type ();

  public:
  struct MCDS_type_pimpl_state
  {
    ::std::string str_;
  };

  MCDS_type_pimpl_state MCDS_type_pimpl_state_;
};

class MultiCellDS_pimpl: public MultiCellDS_pskel
{
  public:
  MultiCellDS_pimpl (bool = false);

  ~MultiCellDS_pimpl ();

  virtual void
  _reset ();

  virtual void
  pre ();

  // Attributes.
  //
  virtual void
  version (const ::std::string&);

  virtual void
  type (const ::MCDS_type&);

  // Elements.
  //
  virtual void
  cell_line (::cell_line::cell_line*);

  virtual void
  metadata (::metadata::metadata*);

  virtual void
  microenvironment (::microenvironment::microenvironment*);

  virtual void
  cellular_information (::cell::cellular_information*);

  virtual ::MultiCellDS*
  post_MultiCellDS ();

  public:
  void
  pre_impl (::MultiCellDS*);

  public:
  struct MultiCellDS_pimpl_state
  {
    ::MultiCellDS* MultiCellDS_;
  };

  MultiCellDS_pimpl_state MultiCellDS_pimpl_state_;
  bool MultiCellDS_pimpl_base_;
};

#ifdef MULTI_CELL_DS_PIMPL_HPP_CLEAR_OMIT_PAGGR
#  undef XSDE_OMIT_PAGGR
#endif

#ifndef XSDE_OMIT_PAGGR

#include <xsde/cxx/hybrid/parser-map.hxx>

// Parser aggregate for the MultiCellDS element.
//
class MultiCellDS_paggr
{
  public:
  MultiCellDS_paggr ();

  void
  pre ()
  {
    this->MultiCellDS_p_.pre ();
  }

  ::MultiCellDS*
  post ()
  {
    return this->MultiCellDS_p_.post_MultiCellDS ();
  }

  ::MultiCellDS_pimpl&
  root_parser ()
  {
    return this->MultiCellDS_p_;
  }

  static const char*
  root_name ();

  static const char*
  root_namespace ();

  void
  reset ()
  {
    this->MultiCellDS_p_._reset ();
  }

  static bool
  polymorphic ()
  {
    return true;
  }

  public:
  ::xml_schema::unsigned_short_pimpl unsigned_short_p_;
  ::xml_schema::int_pimpl int_p_;
  ::xml_schema::unsigned_int_pimpl unsigned_int_p_;
  ::xml_schema::unsigned_long_pimpl unsigned_long_p_;
  ::xml_schema::boolean_pimpl boolean_p_;
  ::xml_schema::double_pimpl double_p_;
  ::xml_schema::string_pimpl string_p_;
  ::xml_schema::date_time_pimpl date_time_p_;
  ::mesh::mesh_pimpl mesh_p_;
  ::basement::basement_membrane_pimpl basement_membrane_p_;
  ::basement::nodes_pimpl nodes_p_;
  ::variables::material_amount_pimpl material_amount_p_;
  ::metadata::data_origin_pimpl data_origin_p_;
  ::common::units_decimal_nonnegative_pimpl units_decimal_nonnegative_p_;
  ::common::units_unsignedShort_pimpl units_unsignedShort_p_;
  ::common::units_fraction_pimpl units_fraction_p_;
  ::common::custom_pimpl custom_p_;
  ::mesh::node_pimpl node_p_;
  ::basement::faces_pimpl faces_p_;
  ::mesh::int_list_xpath_pimpl int_list_xpath_p_;
  ::mesh::bounding_box_pimpl bounding_box_p_;
  ::mesh::list_of_voxels_pimpl list_of_voxels_p_;
  ::metadata::rights_pimpl rights_p_;
  ::metadata::license_pimpl license_p_;
  ::metadata::LicenseDocument_pimpl LicenseDocument_p_;
  ::metadata::metadata_pimpl metadata_p_;
  ::mesh::voxel_pimpl voxel_p_;
  ::common::unsigned_int_list_pimpl unsigned_int_list_p_;
  ::common::data_storage_formats_pimpl data_storage_formats_p_;
  ::common::units_double_list_pimpl units_double_list_p_;
  ::common::two_doubles_pimpl two_doubles_p_;
  ::metadata::orcid_identifier_pimpl orcid_identifier_p_;
  ::metadata::orcid_person_pimpl orcid_person_p_;
  ::metadata::classification_pimpl classification_p_;
  ::metadata::data_analysis_pimpl data_analysis_p_;
  ::metadata::software_pimpl software_p_;
  ::metadata::cell_origin_pimpl cell_origin_p_;
  ::microenvironment::domain_pimpl domain_p_;
  ::metadata::species_pimpl species_p_;
  ::metadata::disease_pimpl disease_p_;
  ::metadata::patient_derived_pimpl patient_derived_p_;
  ::metadata::MultiCellDB_pimpl MultiCellDB_p_;
  ::common::threshold_type_pimpl threshold_type_p_;
  ::metadata::data_origins_pimpl data_origins_p_;
  ::metadata::URL_pimpl URL_p_;
  ::metadata::curation_pimpl curation_p_;
  ::metadata::citation_pimpl citation_p_;
  ::basement::egdes_pimpl egdes_p_;
  ::basement::basement_edge_pimpl basement_edge_p_;
  ::basement::basement_face_pimpl basement_face_p_;
  ::variables::conditions_pimpl conditions_p_;
  ::variables::system_pimpl system_p_;
  ::variables::data_vector_pimpl data_vector_p_;
  ::variables::data_pimpl data_p_;
  ::variables::list_of_variables_pimpl list_of_variables_p_;
  ::variables::transition_threshold_pimpl transition_threshold_p_;
  ::phenotype_common::indentation_observation_pimpl indentation_observation_p_;
  ::phenotype_common::motility_pimpl motility_p_;
  ::phenotype_common::adhesion_pimpl adhesion_p_;
  ::phenotype_common::mechanics_pimpl mechanics_p_;
  ::phenotype_common::friction_pimpl friction_p_;
  ::phenotype_common::rolling_observation_pimpl rolling_observation_p_;
  ::variables::physical_conditions_pimpl physical_conditions_p_;
  ::variables::physical_parameter_set_pimpl physical_parameter_set_p_;
  ::variables::experimental_conditions_pimpl experimental_conditions_p_;
  ::variables::variable_pimpl variable_p_;
  ::variables::amount_type_pimpl amount_type_p_;
  ::common::units_decimal_pimpl units_decimal_p_;
  ::phenotype_common::motility_types_pimpl motility_types_p_;
  ::phenotype_common::timescale_pimpl timescale_p_;
  ::phenotype_common::transport_processes_pimpl transport_processes_p_;
  ::phenotype_common::transport_variable_pimpl transport_variable_p_;
  ::phenotype_common::geometrical_parameters_pimpl geometrical_parameters_p_;
  ::phenotype_common::lengths_pimpl lengths_p_;
  ::phenotype_common::areas_3D_pimpl areas_3D_p_;
  ::phenotype_common::areas_2D_pimpl areas_2D_p_;
  ::phenotype_common::volumes_pimpl volumes_p_;
  ::phenotype_common::geometrical_properties_pimpl geometrical_properties_p_;
  ::phenotype_common::cross_section_pimpl cross_section_p_;
  ::phenotype_common::mass_pimpl mass_p_;
  ::vascular::vascular_node_pimpl vascular_node_p_;
  ::vascular::list_of_vascular_nodes_pimpl list_of_vascular_nodes_p_;
  ::vascular::boundary_node_pimpl boundary_node_p_;
  ::vascular::list_of_boundary_nodes_pimpl list_of_boundary_nodes_p_;
  ::vascular::boundary_conditions_pimpl boundary_conditions_p_;
  ::vascular::boundary_condition_pimpl boundary_condition_p_;
  ::vascular::boundary_type_pimpl boundary_type_p_;
  ::vascular::vascular_segments_pimpl vascular_segments_p_;
  ::vascular::vascular_segment_pimpl vascular_segment_p_;
  ::vascular::endpoint_pimpl endpoint_p_;
  ::vascular::surface_properties_pimpl surface_properties_p_;
  ::vascular::volume_properties_pimpl volume_properties_p_;
  ::vascular::vascular_network_pimpl vascular_network_p_;
  ::microenvironment::microenvironment_pimpl microenvironment_p_;
  ::pkpd::response_observation_pimpl response_observation_p_;
  ::pkpd::PKPD_pimpl PKPD_p_;
  ::phenotype_base::phenotype_base_pimpl phenotype_base_p_;
  ::phenotype_base::expected_timescale_pimpl expected_timescale_p_;
  ::phenotype_base::cell_parts_pimpl cell_parts_p_;
  ::phenotype_base::phenotype_type_pimpl phenotype_type_p_;
  ::cell_cycle::death_rate_type_pimpl death_rate_type_p_;
  ::cell_cycle::cell_cycle_arrest_pimpl cell_cycle_arrest_p_;
  ::cell_cycle::transition_pimpl transition_p_;
  ::cell_cycle::cell_cycle_phase_pimpl cell_cycle_phase_p_;
  ::cell_cycle::summary_elements_pimpl summary_elements_p_;
  ::cell_cycle::cell_cycle_pimpl cell_cycle_p_;
  ::cell_cycle::cell_death_pimpl cell_death_p_;
  ::cell_cycle::death_type_pimpl death_type_p_;
  ::cell_cycle::arrest_condition_pimpl arrest_condition_p_;
  ::cell_cycle::arrest_type_pimpl arrest_type_p_;
  ::pkpd::therapy_pimpl therapy_p_;
  ::pkpd::pharmacodynamics_pimpl pharmacodynamics_p_;
  ::pkpd::therapy_measurement_set_pimpl therapy_measurement_set_p_;
  ::pkpd::response_pimpl response_p_;
  ::pkpd::pharmacokinetics_pimpl pharmacokinetics_p_;
  ::pkpd::drug_dose_pimpl drug_dose_p_;
  ::pkpd::dose_pimpl dose_p_;
  ::pkpd::drug_pk_pimpl drug_pk_p_;
  ::phenotype::phenotype_pimpl phenotype_p_;
  ::phenotype_dataset::phenotype_dataset_pimpl phenotype_dataset_p_;
  ::cell_line::cell_line_pimpl cell_line_p_;
  ::cell_line::DCLs_pimpl DCLs_p_;
  ::state::list_of_adhered_cells_pimpl list_of_adhered_cells_p_;
  ::cell::population_definition_pimpl population_definition_p_;
  ::cell::population_definitions_pimpl population_definitions_p_;
  ::cell::cell_pimpl cell_p_;
  ::cell::cell_population_individual_pimpl cell_population_individual_p_;
  ::cell::cell_population_aggregate_pimpl cell_population_aggregate_p_;
  ::cell::population_vector_pimpl population_vector_p_;
  ::cell::cell_populations_pimpl cell_populations_p_;
  ::cell::cellular_information_pimpl cellular_information_p_;
  ::MultiCellDS_pimpl MultiCellDS_p_;
  ::MCDS_type_pimpl MCDS_type_p_;
  ::state::state_pimpl state_p_;
  ::state::cell_parts_pimpl cell_parts_p1_;
  ::state::orientation_formalism_pimpl orientation_formalism_p_;
  ::state::phase_pimpl phase_p_;
  ::state::adhered_cell_pimpl adhered_cell_p_;
  ::state::phase_name_pimpl phase_name_p_;
  ::state::orientation_pimpl orientation_p_;

  ::xsde::cxx::hybrid::parser_map_impl parser_map_;
  ::xsde::cxx::hybrid::parser_map_impl::entry parser_map_entries_[1UL];
};

#endif // XSDE_OMIT_PAGGR

// Begin epilogue.
//
//
// End epilogue.

#include <xsde/cxx/post.hxx>

#endif // MULTI_CELL_DS_PIMPL_HPP
