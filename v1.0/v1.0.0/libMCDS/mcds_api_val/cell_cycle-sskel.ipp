// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

namespace cell_cycle
{
  // death_type_sskel
  //

  inline
  death_type_sskel::
  death_type_sskel (::xml_schema::string_sskel* tiein)
  : ::xml_schema::string_sskel (tiein, 0),
    death_type_impl_ (0)
  {
    this->_enumeration_facet (_xsde_death_type_sskel_enums_, 3UL);
  }

  inline
  death_type_sskel::
  death_type_sskel (death_type_sskel* impl, void*)
  : ::xml_schema::string_sskel (impl, 0),
    death_type_impl_ (impl)
  {
    this->_enumeration_facet (_xsde_death_type_sskel_enums_, 3UL);
  }

  // death_rate_type_sskel
  //

  inline
  void death_rate_type_sskel::
  type_serializer (::cell_cycle::death_type_sskel& type)
  {
    this->type_serializer_ = &type;
  }

  inline
  void death_rate_type_sskel::
  serializers (::xml_schema::string_sskel& units,
               ::xml_schema::string_sskel& measurement_type,
               ::xml_schema::double_sskel& uncertainty,
               ::xml_schema::double_sskel& negative_uncertainty,
               ::xml_schema::double_sskel& positive_uncertainty,
               ::xml_schema::double_sskel& uncertainty_percentage,
               ::xml_schema::double_sskel& negative_uncertainty_percentage,
               ::xml_schema::double_sskel& positive_uncertainty_percentage,
               ::xml_schema::double_sskel& median,
               ::xml_schema::double_sskel& standard_deviation,
               ::common::two_doubles_sskel& interquartile_range,
               ::common::two_doubles_sskel& range,
               ::xml_schema::double_sskel& min,
               ::xml_schema::double_sskel& max,
               ::xml_schema::double_sskel& standard_error,
               ::xml_schema::double_sskel& standard_error_of_the_mean,
               ::xml_schema::int_sskel& number_obs,
               ::xml_schema::double_sskel& skewnesss,
               ::xml_schema::double_sskel& kurtosis,
               ::cell_cycle::death_type_sskel& type)
  {
    this->units_serializer_ = &units;
    this->measurement_type_serializer_ = &measurement_type;
    this->uncertainty_serializer_ = &uncertainty;
    this->negative_uncertainty_serializer_ = &negative_uncertainty;
    this->positive_uncertainty_serializer_ = &positive_uncertainty;
    this->uncertainty_percentage_serializer_ = &uncertainty_percentage;
    this->negative_uncertainty_percentage_serializer_ = &negative_uncertainty_percentage;
    this->positive_uncertainty_percentage_serializer_ = &positive_uncertainty_percentage;
    this->median_serializer_ = &median;
    this->standard_deviation_serializer_ = &standard_deviation;
    this->interquartile_range_serializer_ = &interquartile_range;
    this->range_serializer_ = &range;
    this->min_serializer_ = &min;
    this->max_serializer_ = &max;
    this->standard_error_serializer_ = &standard_error;
    this->standard_error_of_the_mean_serializer_ = &standard_error_of_the_mean;
    this->number_obs_serializer_ = &number_obs;
    this->skewnesss_serializer_ = &skewnesss;
    this->kurtosis_serializer_ = &kurtosis;
    this->type_serializer_ = &type;
  }

  inline
  death_rate_type_sskel::
  death_rate_type_sskel (::common::units_decimal_nonnegative_sskel* tiein)
  : ::common::units_decimal_nonnegative_sskel (tiein, 0),
    death_rate_type_impl_ (0),
    type_serializer_ (0)
  {
  }

  inline
  death_rate_type_sskel::
  death_rate_type_sskel (death_rate_type_sskel* impl, void*)
  : ::common::units_decimal_nonnegative_sskel (impl, 0),
    death_rate_type_impl_ (impl),
    type_serializer_ (0)
  {
  }

  // cell_cycle_arrest_sskel
  //

  inline
  void cell_cycle_arrest_sskel::
  condition_serializer (::cell_cycle::arrest_condition_sskel& s)
  {
    this->condition_serializer_ = &s;
  }

  inline
  void cell_cycle_arrest_sskel::
  condition_serializer (::xml_schema::serializer_map& m)
  {
    this->condition_serializer_map_ = &m;
  }

  inline
  void cell_cycle_arrest_sskel::
  serializers (::cell_cycle::arrest_condition_sskel& condition)
  {
    this->condition_serializer_ = &condition;
  }

  inline
  void cell_cycle_arrest_sskel::
  serializer_maps (::xml_schema::serializer_map& condition)
  {
    this->condition_serializer_map_ = &condition;
  }

  inline
  cell_cycle_arrest_sskel::
  cell_cycle_arrest_sskel ()
  : cell_cycle_arrest_impl_ (0),
    condition_serializer_ (0),
    condition_serializer_map_ (0)
  {
  }

  inline
  cell_cycle_arrest_sskel::
  cell_cycle_arrest_sskel (cell_cycle_arrest_sskel* impl, void*)
  : ::xsde::cxx::serializer::validating::complex_content (impl, 0),
    cell_cycle_arrest_impl_ (impl),
    condition_serializer_ (0),
    condition_serializer_map_ (0)
  {
  }

  // transition_sskel
  //

  inline
  void transition_sskel::
  checkpoint_failure_probability_serializer (::common::units_decimal_sskel& s)
  {
    this->checkpoint_failure_probability_serializer_ = &s;
  }

  inline
  void transition_sskel::
  checkpoint_failure_probability_serializer (::xml_schema::serializer_map& m)
  {
    this->checkpoint_failure_probability_serializer_map_ = &m;
  }

  inline
  void transition_sskel::
  subsequent_phase_serializer (::xml_schema::unsigned_long_sskel& s)
  {
    this->subsequent_phase_serializer_ = &s;
  }

  inline
  void transition_sskel::
  subsequent_phase_serializer (::xml_schema::serializer_map& m)
  {
    this->subsequent_phase_serializer_map_ = &m;
  }

  inline
  void transition_sskel::
  threshold_serializer (::variables::transition_threshold_sskel& s)
  {
    this->threshold_serializer_ = &s;
  }

  inline
  void transition_sskel::
  threshold_serializer (::xml_schema::serializer_map& m)
  {
    this->threshold_serializer_map_ = &m;
  }

  inline
  void transition_sskel::
  transition_rate_serializer (::common::units_decimal_sskel& s)
  {
    this->transition_rate_serializer_ = &s;
  }

  inline
  void transition_sskel::
  transition_rate_serializer (::xml_schema::serializer_map& m)
  {
    this->transition_rate_serializer_map_ = &m;
  }

  inline
  void transition_sskel::
  serializers (::common::units_decimal_sskel& checkpoint_failure_probability,
               ::xml_schema::unsigned_long_sskel& subsequent_phase,
               ::variables::transition_threshold_sskel& threshold,
               ::common::units_decimal_sskel& transition_rate)
  {
    this->checkpoint_failure_probability_serializer_ = &checkpoint_failure_probability;
    this->subsequent_phase_serializer_ = &subsequent_phase;
    this->threshold_serializer_ = &threshold;
    this->transition_rate_serializer_ = &transition_rate;
  }

  inline
  void transition_sskel::
  serializer_maps (::xml_schema::serializer_map& checkpoint_failure_probability,
                   ::xml_schema::serializer_map& subsequent_phase,
                   ::xml_schema::serializer_map& threshold,
                   ::xml_schema::serializer_map& transition_rate)
  {
    this->checkpoint_failure_probability_serializer_map_ = &checkpoint_failure_probability;
    this->subsequent_phase_serializer_map_ = &subsequent_phase;
    this->threshold_serializer_map_ = &threshold;
    this->transition_rate_serializer_map_ = &transition_rate;
  }

  inline
  transition_sskel::
  transition_sskel ()
  : transition_impl_ (0),
    checkpoint_failure_probability_serializer_ (0),
    checkpoint_failure_probability_serializer_map_ (0),
    subsequent_phase_serializer_ (0),
    subsequent_phase_serializer_map_ (0),
    threshold_serializer_ (0),
    threshold_serializer_map_ (0),
    transition_rate_serializer_ (0),
    transition_rate_serializer_map_ (0)
  {
  }

  inline
  transition_sskel::
  transition_sskel (transition_sskel* impl, void*)
  : ::xsde::cxx::serializer::validating::complex_content (impl, 0),
    transition_impl_ (impl),
    checkpoint_failure_probability_serializer_ (0),
    checkpoint_failure_probability_serializer_map_ (0),
    subsequent_phase_serializer_ (0),
    subsequent_phase_serializer_map_ (0),
    threshold_serializer_ (0),
    threshold_serializer_map_ (0),
    transition_rate_serializer_ (0),
    transition_rate_serializer_map_ (0)
  {
  }

  // cell_cycle_phase_sskel
  //

  inline
  void cell_cycle_phase_sskel::
  name_serializer (::xml_schema::string_sskel& name)
  {
    this->name_serializer_ = &name;
  }

  inline
  void cell_cycle_phase_sskel::
  ID_serializer (::xml_schema::unsigned_long_sskel& ID)
  {
    this->ID_serializer_ = &ID;
  }

  inline
  void cell_cycle_phase_sskel::
  birth_rate_serializer (::common::units_decimal_nonnegative_sskel& s)
  {
    this->birth_rate_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  birth_rate_serializer (::xml_schema::serializer_map& m)
  {
    this->birth_rate_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  duration_serializer (::common::units_decimal_nonnegative_sskel& s)
  {
    this->duration_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  duration_serializer (::xml_schema::serializer_map& m)
  {
    this->duration_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  death_rate_serializer (::cell_cycle::death_rate_type_sskel& s)
  {
    this->death_rate_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  death_rate_serializer (::xml_schema::serializer_map& m)
  {
    this->death_rate_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  net_birth_rate_serializer (::common::units_decimal_sskel& s)
  {
    this->net_birth_rate_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  net_birth_rate_serializer (::xml_schema::serializer_map& m)
  {
    this->net_birth_rate_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  population_doubling_time_serializer (::common::units_decimal_nonnegative_sskel& s)
  {
    this->population_doubling_time_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  population_doubling_time_serializer (::xml_schema::serializer_map& m)
  {
    this->population_doubling_time_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  cell_cycle_arrest_serializer (::cell_cycle::cell_cycle_arrest_sskel& s)
  {
    this->cell_cycle_arrest_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  cell_cycle_arrest_serializer (::xml_schema::serializer_map& m)
  {
    this->cell_cycle_arrest_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  transition_serializer (::cell_cycle::transition_sskel& s)
  {
    this->transition_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  transition_serializer (::xml_schema::serializer_map& m)
  {
    this->transition_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  cell_part_serializer (::phenotype_base::cell_parts_sskel& s)
  {
    this->cell_part_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  cell_part_serializer (::xml_schema::serializer_map& m)
  {
    this->cell_part_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  custom_serializer (::common::custom_sskel& s)
  {
    this->custom_serializer_ = &s;
  }

  inline
  void cell_cycle_phase_sskel::
  custom_serializer (::xml_schema::serializer_map& m)
  {
    this->custom_serializer_map_ = &m;
  }

  inline
  void cell_cycle_phase_sskel::
  serializers (::xml_schema::string_sskel& name,
               ::xml_schema::unsigned_long_sskel& ID,
               ::common::units_decimal_nonnegative_sskel& birth_rate,
               ::common::units_decimal_nonnegative_sskel& duration,
               ::cell_cycle::death_rate_type_sskel& death_rate,
               ::common::units_decimal_sskel& net_birth_rate,
               ::common::units_decimal_nonnegative_sskel& population_doubling_time,
               ::cell_cycle::cell_cycle_arrest_sskel& cell_cycle_arrest,
               ::cell_cycle::transition_sskel& transition,
               ::phenotype_base::cell_parts_sskel& cell_part,
               ::common::custom_sskel& custom)
  {
    this->name_serializer_ = &name;
    this->ID_serializer_ = &ID;
    this->birth_rate_serializer_ = &birth_rate;
    this->duration_serializer_ = &duration;
    this->death_rate_serializer_ = &death_rate;
    this->net_birth_rate_serializer_ = &net_birth_rate;
    this->population_doubling_time_serializer_ = &population_doubling_time;
    this->cell_cycle_arrest_serializer_ = &cell_cycle_arrest;
    this->transition_serializer_ = &transition;
    this->cell_part_serializer_ = &cell_part;
    this->custom_serializer_ = &custom;
  }

  inline
  void cell_cycle_phase_sskel::
  serializer_maps (::xml_schema::serializer_map& birth_rate,
                   ::xml_schema::serializer_map& duration,
                   ::xml_schema::serializer_map& death_rate,
                   ::xml_schema::serializer_map& net_birth_rate,
                   ::xml_schema::serializer_map& population_doubling_time,
                   ::xml_schema::serializer_map& cell_cycle_arrest,
                   ::xml_schema::serializer_map& transition,
                   ::xml_schema::serializer_map& cell_part,
                   ::xml_schema::serializer_map& custom)
  {
    this->birth_rate_serializer_map_ = &birth_rate;
    this->duration_serializer_map_ = &duration;
    this->death_rate_serializer_map_ = &death_rate;
    this->net_birth_rate_serializer_map_ = &net_birth_rate;
    this->population_doubling_time_serializer_map_ = &population_doubling_time;
    this->cell_cycle_arrest_serializer_map_ = &cell_cycle_arrest;
    this->transition_serializer_map_ = &transition;
    this->cell_part_serializer_map_ = &cell_part;
    this->custom_serializer_map_ = &custom;
  }

  inline
  cell_cycle_phase_sskel::
  cell_cycle_phase_sskel ()
  : cell_cycle_phase_impl_ (0),
    name_serializer_ (0),
    ID_serializer_ (0),
    birth_rate_serializer_ (0),
    birth_rate_serializer_map_ (0),
    duration_serializer_ (0),
    duration_serializer_map_ (0),
    death_rate_serializer_ (0),
    death_rate_serializer_map_ (0),
    net_birth_rate_serializer_ (0),
    net_birth_rate_serializer_map_ (0),
    population_doubling_time_serializer_ (0),
    population_doubling_time_serializer_map_ (0),
    cell_cycle_arrest_serializer_ (0),
    cell_cycle_arrest_serializer_map_ (0),
    transition_serializer_ (0),
    transition_serializer_map_ (0),
    cell_part_serializer_ (0),
    cell_part_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  inline
  cell_cycle_phase_sskel::
  cell_cycle_phase_sskel (cell_cycle_phase_sskel* impl, void*)
  : ::xsde::cxx::serializer::validating::complex_content (impl, 0),
    cell_cycle_phase_impl_ (impl),
    name_serializer_ (0),
    ID_serializer_ (0),
    birth_rate_serializer_ (0),
    birth_rate_serializer_map_ (0),
    duration_serializer_ (0),
    duration_serializer_map_ (0),
    death_rate_serializer_ (0),
    death_rate_serializer_map_ (0),
    net_birth_rate_serializer_ (0),
    net_birth_rate_serializer_map_ (0),
    population_doubling_time_serializer_ (0),
    population_doubling_time_serializer_map_ (0),
    cell_cycle_arrest_serializer_ (0),
    cell_cycle_arrest_serializer_map_ (0),
    transition_serializer_ (0),
    transition_serializer_map_ (0),
    cell_part_serializer_ (0),
    cell_part_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  // summary_elements_sskel
  //

  inline
  void summary_elements_sskel::
  birth_rate_serializer (::common::units_decimal_nonnegative_sskel& s)
  {
    this->birth_rate_serializer_ = &s;
  }

  inline
  void summary_elements_sskel::
  birth_rate_serializer (::xml_schema::serializer_map& m)
  {
    this->birth_rate_serializer_map_ = &m;
  }

  inline
  void summary_elements_sskel::
  duration_serializer (::common::units_decimal_nonnegative_sskel& s)
  {
    this->duration_serializer_ = &s;
  }

  inline
  void summary_elements_sskel::
  duration_serializer (::xml_schema::serializer_map& m)
  {
    this->duration_serializer_map_ = &m;
  }

  inline
  void summary_elements_sskel::
  death_rate_serializer (::cell_cycle::death_rate_type_sskel& s)
  {
    this->death_rate_serializer_ = &s;
  }

  inline
  void summary_elements_sskel::
  death_rate_serializer (::xml_schema::serializer_map& m)
  {
    this->death_rate_serializer_map_ = &m;
  }

  inline
  void summary_elements_sskel::
  net_birth_rate_serializer (::common::units_decimal_sskel& s)
  {
    this->net_birth_rate_serializer_ = &s;
  }

  inline
  void summary_elements_sskel::
  net_birth_rate_serializer (::xml_schema::serializer_map& m)
  {
    this->net_birth_rate_serializer_map_ = &m;
  }

  inline
  void summary_elements_sskel::
  population_doubling_time_serializer (::common::units_decimal_nonnegative_sskel& s)
  {
    this->population_doubling_time_serializer_ = &s;
  }

  inline
  void summary_elements_sskel::
  population_doubling_time_serializer (::xml_schema::serializer_map& m)
  {
    this->population_doubling_time_serializer_map_ = &m;
  }

  inline
  void summary_elements_sskel::
  serializers (::common::units_decimal_nonnegative_sskel& birth_rate,
               ::common::units_decimal_nonnegative_sskel& duration,
               ::cell_cycle::death_rate_type_sskel& death_rate,
               ::common::units_decimal_sskel& net_birth_rate,
               ::common::units_decimal_nonnegative_sskel& population_doubling_time)
  {
    this->birth_rate_serializer_ = &birth_rate;
    this->duration_serializer_ = &duration;
    this->death_rate_serializer_ = &death_rate;
    this->net_birth_rate_serializer_ = &net_birth_rate;
    this->population_doubling_time_serializer_ = &population_doubling_time;
  }

  inline
  void summary_elements_sskel::
  serializer_maps (::xml_schema::serializer_map& birth_rate,
                   ::xml_schema::serializer_map& duration,
                   ::xml_schema::serializer_map& death_rate,
                   ::xml_schema::serializer_map& net_birth_rate,
                   ::xml_schema::serializer_map& population_doubling_time)
  {
    this->birth_rate_serializer_map_ = &birth_rate;
    this->duration_serializer_map_ = &duration;
    this->death_rate_serializer_map_ = &death_rate;
    this->net_birth_rate_serializer_map_ = &net_birth_rate;
    this->population_doubling_time_serializer_map_ = &population_doubling_time;
  }

  inline
  summary_elements_sskel::
  summary_elements_sskel ()
  : summary_elements_impl_ (0),
    birth_rate_serializer_ (0),
    birth_rate_serializer_map_ (0),
    duration_serializer_ (0),
    duration_serializer_map_ (0),
    death_rate_serializer_ (0),
    death_rate_serializer_map_ (0),
    net_birth_rate_serializer_ (0),
    net_birth_rate_serializer_map_ (0),
    population_doubling_time_serializer_ (0),
    population_doubling_time_serializer_map_ (0)
  {
  }

  inline
  summary_elements_sskel::
  summary_elements_sskel (summary_elements_sskel* impl, void*)
  : ::xsde::cxx::serializer::validating::complex_content (impl, 0),
    summary_elements_impl_ (impl),
    birth_rate_serializer_ (0),
    birth_rate_serializer_map_ (0),
    duration_serializer_ (0),
    duration_serializer_map_ (0),
    death_rate_serializer_ (0),
    death_rate_serializer_map_ (0),
    net_birth_rate_serializer_ (0),
    net_birth_rate_serializer_map_ (0),
    population_doubling_time_serializer_ (0),
    population_doubling_time_serializer_map_ (0)
  {
  }

  // cell_cycle_sskel
  //

  inline
  void cell_cycle_sskel::
  model_serializer (::xml_schema::string_sskel& model)
  {
    this->model_serializer_ = &model;
  }

  inline
  void cell_cycle_sskel::
  ID_serializer (::xml_schema::unsigned_long_sskel& ID)
  {
    this->ID_serializer_ = &ID;
  }

  inline
  void cell_cycle_sskel::
  cell_cycle_phase_serializer (::cell_cycle::cell_cycle_phase_sskel& s)
  {
    this->cell_cycle_phase_serializer_ = &s;
  }

  inline
  void cell_cycle_sskel::
  cell_cycle_phase_serializer (::xml_schema::serializer_map& m)
  {
    this->cell_cycle_phase_serializer_map_ = &m;
  }

  inline
  void cell_cycle_sskel::
  cell_death_serializer (::cell_cycle::cell_death_sskel& s)
  {
    this->cell_death_serializer_ = &s;
  }

  inline
  void cell_cycle_sskel::
  cell_death_serializer (::xml_schema::serializer_map& m)
  {
    this->cell_death_serializer_map_ = &m;
  }

  inline
  void cell_cycle_sskel::
  summary_elements_serializer (::cell_cycle::summary_elements_sskel& s)
  {
    this->summary_elements_serializer_ = &s;
  }

  inline
  void cell_cycle_sskel::
  summary_elements_serializer (::xml_schema::serializer_map& m)
  {
    this->summary_elements_serializer_map_ = &m;
  }

  inline
  void cell_cycle_sskel::
  custom_serializer (::common::custom_sskel& s)
  {
    this->custom_serializer_ = &s;
  }

  inline
  void cell_cycle_sskel::
  custom_serializer (::xml_schema::serializer_map& m)
  {
    this->custom_serializer_map_ = &m;
  }

  inline
  void cell_cycle_sskel::
  serializers (::xml_schema::string_sskel& model,
               ::xml_schema::unsigned_long_sskel& ID,
               ::cell_cycle::cell_cycle_phase_sskel& cell_cycle_phase,
               ::cell_cycle::cell_death_sskel& cell_death,
               ::cell_cycle::summary_elements_sskel& summary_elements,
               ::common::custom_sskel& custom)
  {
    this->model_serializer_ = &model;
    this->ID_serializer_ = &ID;
    this->cell_cycle_phase_serializer_ = &cell_cycle_phase;
    this->cell_death_serializer_ = &cell_death;
    this->summary_elements_serializer_ = &summary_elements;
    this->custom_serializer_ = &custom;
  }

  inline
  void cell_cycle_sskel::
  serializer_maps (::xml_schema::serializer_map& cell_cycle_phase,
                   ::xml_schema::serializer_map& cell_death,
                   ::xml_schema::serializer_map& summary_elements,
                   ::xml_schema::serializer_map& custom)
  {
    this->cell_cycle_phase_serializer_map_ = &cell_cycle_phase;
    this->cell_death_serializer_map_ = &cell_death;
    this->summary_elements_serializer_map_ = &summary_elements;
    this->custom_serializer_map_ = &custom;
  }

  inline
  cell_cycle_sskel::
  cell_cycle_sskel ()
  : cell_cycle_impl_ (0),
    model_serializer_ (0),
    ID_serializer_ (0),
    cell_cycle_phase_serializer_ (0),
    cell_cycle_phase_serializer_map_ (0),
    cell_death_serializer_ (0),
    cell_death_serializer_map_ (0),
    summary_elements_serializer_ (0),
    summary_elements_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  inline
  cell_cycle_sskel::
  cell_cycle_sskel (cell_cycle_sskel* impl, void*)
  : ::xsde::cxx::serializer::validating::complex_content (impl, 0),
    cell_cycle_impl_ (impl),
    model_serializer_ (0),
    ID_serializer_ (0),
    cell_cycle_phase_serializer_ (0),
    cell_cycle_phase_serializer_map_ (0),
    cell_death_serializer_ (0),
    cell_death_serializer_map_ (0),
    summary_elements_serializer_ (0),
    summary_elements_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  // cell_death_sskel
  //

  inline
  void cell_death_sskel::
  type_serializer (::cell_cycle::death_type_sskel& type)
  {
    this->type_serializer_ = &type;
  }

  inline
  void cell_death_sskel::
  ID_serializer (::xml_schema::unsigned_long_sskel& ID)
  {
    this->ID_serializer_ = &ID;
  }

  inline
  void cell_death_sskel::
  duration_serializer (::common::units_decimal_sskel& s)
  {
    this->duration_serializer_ = &s;
  }

  inline
  void cell_death_sskel::
  duration_serializer (::xml_schema::serializer_map& m)
  {
    this->duration_serializer_map_ = &m;
  }

  inline
  void cell_death_sskel::
  cell_part_serializer (::phenotype_base::cell_parts_sskel& s)
  {
    this->cell_part_serializer_ = &s;
  }

  inline
  void cell_death_sskel::
  cell_part_serializer (::xml_schema::serializer_map& m)
  {
    this->cell_part_serializer_map_ = &m;
  }

  inline
  void cell_death_sskel::
  custom_serializer (::common::custom_sskel& s)
  {
    this->custom_serializer_ = &s;
  }

  inline
  void cell_death_sskel::
  custom_serializer (::xml_schema::serializer_map& m)
  {
    this->custom_serializer_map_ = &m;
  }

  inline
  void cell_death_sskel::
  serializers (::cell_cycle::death_type_sskel& type,
               ::xml_schema::unsigned_long_sskel& ID,
               ::common::units_decimal_sskel& duration,
               ::phenotype_base::cell_parts_sskel& cell_part,
               ::common::custom_sskel& custom)
  {
    this->type_serializer_ = &type;
    this->ID_serializer_ = &ID;
    this->duration_serializer_ = &duration;
    this->cell_part_serializer_ = &cell_part;
    this->custom_serializer_ = &custom;
  }

  inline
  void cell_death_sskel::
  serializer_maps (::xml_schema::serializer_map& duration,
                   ::xml_schema::serializer_map& cell_part,
                   ::xml_schema::serializer_map& custom)
  {
    this->duration_serializer_map_ = &duration;
    this->cell_part_serializer_map_ = &cell_part;
    this->custom_serializer_map_ = &custom;
  }

  inline
  cell_death_sskel::
  cell_death_sskel ()
  : cell_death_impl_ (0),
    type_serializer_ (0),
    ID_serializer_ (0),
    duration_serializer_ (0),
    duration_serializer_map_ (0),
    cell_part_serializer_ (0),
    cell_part_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  inline
  cell_death_sskel::
  cell_death_sskel (cell_death_sskel* impl, void*)
  : ::xsde::cxx::serializer::validating::complex_content (impl, 0),
    cell_death_impl_ (impl),
    type_serializer_ (0),
    ID_serializer_ (0),
    duration_serializer_ (0),
    duration_serializer_map_ (0),
    cell_part_serializer_ (0),
    cell_part_serializer_map_ (0),
    custom_serializer_ (0),
    custom_serializer_map_ (0)
  {
  }

  // arrest_type_sskel
  //

  inline
  arrest_type_sskel::
  arrest_type_sskel (::xml_schema::string_sskel* tiein)
  : ::xml_schema::string_sskel (tiein, 0),
    arrest_type_impl_ (0)
  {
    this->_enumeration_facet (_xsde_arrest_type_sskel_enums_, 6UL);
  }

  inline
  arrest_type_sskel::
  arrest_type_sskel (arrest_type_sskel* impl, void*)
  : ::xml_schema::string_sskel (impl, 0),
    arrest_type_impl_ (impl)
  {
    this->_enumeration_facet (_xsde_arrest_type_sskel_enums_, 6UL);
  }

  // arrest_condition_sskel
  //

  inline
  void arrest_condition_sskel::
  type_serializer (::cell_cycle::arrest_type_sskel& type)
  {
    this->type_serializer_ = &type;
  }

  inline
  void arrest_condition_sskel::
  serializers (::xml_schema::string_sskel& units,
               ::xml_schema::string_sskel& measurement_type,
               ::xml_schema::double_sskel& uncertainty,
               ::xml_schema::double_sskel& negative_uncertainty,
               ::xml_schema::double_sskel& positive_uncertainty,
               ::xml_schema::double_sskel& uncertainty_percentage,
               ::xml_schema::double_sskel& negative_uncertainty_percentage,
               ::xml_schema::double_sskel& positive_uncertainty_percentage,
               ::xml_schema::double_sskel& median,
               ::xml_schema::double_sskel& standard_deviation,
               ::common::two_doubles_sskel& interquartile_range,
               ::common::two_doubles_sskel& range,
               ::xml_schema::double_sskel& min,
               ::xml_schema::double_sskel& max,
               ::xml_schema::double_sskel& standard_error,
               ::xml_schema::double_sskel& standard_error_of_the_mean,
               ::xml_schema::int_sskel& number_obs,
               ::xml_schema::double_sskel& skewnesss,
               ::xml_schema::double_sskel& kurtosis,
               ::cell_cycle::arrest_type_sskel& type)
  {
    this->units_serializer_ = &units;
    this->measurement_type_serializer_ = &measurement_type;
    this->uncertainty_serializer_ = &uncertainty;
    this->negative_uncertainty_serializer_ = &negative_uncertainty;
    this->positive_uncertainty_serializer_ = &positive_uncertainty;
    this->uncertainty_percentage_serializer_ = &uncertainty_percentage;
    this->negative_uncertainty_percentage_serializer_ = &negative_uncertainty_percentage;
    this->positive_uncertainty_percentage_serializer_ = &positive_uncertainty_percentage;
    this->median_serializer_ = &median;
    this->standard_deviation_serializer_ = &standard_deviation;
    this->interquartile_range_serializer_ = &interquartile_range;
    this->range_serializer_ = &range;
    this->min_serializer_ = &min;
    this->max_serializer_ = &max;
    this->standard_error_serializer_ = &standard_error;
    this->standard_error_of_the_mean_serializer_ = &standard_error_of_the_mean;
    this->number_obs_serializer_ = &number_obs;
    this->skewnesss_serializer_ = &skewnesss;
    this->kurtosis_serializer_ = &kurtosis;
    this->type_serializer_ = &type;
  }

  inline
  arrest_condition_sskel::
  arrest_condition_sskel (::common::units_decimal_sskel* tiein)
  : ::common::units_decimal_sskel (tiein, 0),
    arrest_condition_impl_ (0),
    type_serializer_ (0)
  {
  }

  inline
  arrest_condition_sskel::
  arrest_condition_sskel (arrest_condition_sskel* impl, void*)
  : ::common::units_decimal_sskel (impl, 0),
    arrest_condition_impl_ (impl),
    type_serializer_ (0)
  {
  }

  // cycles_and_deaths_sskel
  //

  inline
  void cycles_and_deaths_sskel::
  cell_cycle_serializer (::cell_cycle::cell_cycle_sskel& s)
  {
    this->cell_cycle_serializer_ = &s;
  }

  inline
  void cycles_and_deaths_sskel::
  cell_cycle_serializer (::xml_schema::serializer_map& m)
  {
    this->cell_cycle_serializer_map_ = &m;
  }

  inline
  void cycles_and_deaths_sskel::
  cell_death_serializer (::cell_cycle::cell_death_sskel& s)
  {
    this->cell_death_serializer_ = &s;
  }

  inline
  void cycles_and_deaths_sskel::
  cell_death_serializer (::xml_schema::serializer_map& m)
  {
    this->cell_death_serializer_map_ = &m;
  }

  inline
  void cycles_and_deaths_sskel::
  serializers (::cell_cycle::cell_cycle_sskel& cell_cycle,
               ::cell_cycle::cell_death_sskel& cell_death)
  {
    this->cell_cycle_serializer_ = &cell_cycle;
    this->cell_death_serializer_ = &cell_death;
  }

  inline
  void cycles_and_deaths_sskel::
  serializer_maps (::xml_schema::serializer_map& cell_cycle,
                   ::xml_schema::serializer_map& cell_death)
  {
    this->cell_cycle_serializer_map_ = &cell_cycle;
    this->cell_death_serializer_map_ = &cell_death;
  }

  inline
  cycles_and_deaths_sskel::
  cycles_and_deaths_sskel ()
  : cycles_and_deaths_impl_ (0),
    cell_cycle_serializer_ (0),
    cell_cycle_serializer_map_ (0),
    cell_death_serializer_ (0),
    cell_death_serializer_map_ (0)
  {
  }

  inline
  cycles_and_deaths_sskel::
  cycles_and_deaths_sskel (cycles_and_deaths_sskel* impl, void*)
  : ::xsde::cxx::serializer::validating::complex_content (impl, 0),
    cycles_and_deaths_impl_ (impl),
    cell_cycle_serializer_ (0),
    cell_cycle_serializer_map_ (0),
    cell_death_serializer_ (0),
    cell_death_serializer_map_ (0)
  {
  }
}

// Begin epilogue.
//
//
// End epilogue.

