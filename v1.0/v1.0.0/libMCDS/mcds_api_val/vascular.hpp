// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef VASCULAR_HPP
#define VASCULAR_HPP

#include <xsde/cxx/version.hxx>

#if (XSDE_INT_VERSION != 3020000L)
#error XSD/e runtime version mismatch
#endif

#include <xsde/cxx/config.hxx>

#ifndef XSDE_ENCODING_UTF8
#error the generated code uses the UTF-8 encodingwhile the XSD/e runtime does not (reconfigure the runtime or change the --char-encoding value)
#endif

#ifndef XSDE_STL
#error the generated code uses STL while the XSD/e runtime does not (reconfigure the runtime or add --no-stl)
#endif

#ifndef XSDE_EXCEPTIONS
#error the generated code uses exceptions while the XSD/e runtime does not (reconfigure the runtime or add --no-exceptions)
#endif

#ifndef XSDE_LONGLONG
#error the generated code uses long long while the XSD/e runtime does not (reconfigure the runtime or add --no-long-long)
#endif

#ifdef XSDE_CUSTOM_ALLOCATOR
#error the XSD/e runtime uses custom allocator while the generated code does not (reconfigure the runtime or add --custom-allocator)
#endif

#include <xsde/cxx/pre.hxx>

// Begin prologue.
//
//
// End prologue.

#include "vascular-fwd.hpp"

#ifndef XSDE_DONT_INCLUDE_INLINE
#define XSDE_DONT_INCLUDE_INLINE

#include "common.hpp"

#include "mesh.hpp"

#include "variables.hpp"

#include "phenotype_common.hpp"

#undef XSDE_DONT_INCLUDE_INLINE
#else

#include "common.hpp"

#include "mesh.hpp"

#include "variables.hpp"

#include "phenotype_common.hpp"

#endif // XSDE_DONT_INCLUDE_INLINE

namespace vascular
{
  // vascular_node (variable-length)
  //
  class vascular_node: public ::mesh::node
  {
    private:
    vascular_node (const vascular_node&);
    vascular_node& operator= (const vascular_node&);

    public:
    vascular_node ();

    virtual vascular_node*
    _clone () const;

    virtual
    ~vascular_node ();

    // boundary_node
    //
    bool
    boundary_node_present () const;

    void
    boundary_node_present (bool);

    bool
    boundary_node () const;

    bool&
    boundary_node ();

    void
    boundary_node (bool);

    // Type information.
    //
    static const ::std::string&
    _static_type ();

    virtual const ::std::string&
    _dynamic_type () const;

    void
    _copy (vascular_node&) const;

    private:
    bool boundary_node_;
    unsigned char boundary_node_present_;
  };

  // list_of_vascular_nodes (variable-length)
  //
  class list_of_vascular_nodes
  {
    private:
    list_of_vascular_nodes (const list_of_vascular_nodes&);
    list_of_vascular_nodes& operator= (const list_of_vascular_nodes&);

    public:
    list_of_vascular_nodes ();

    list_of_vascular_nodes*
    _clone () const;

    ~list_of_vascular_nodes ();

    // vascular_node
    //
    typedef ::xsde::cxx::hybrid::var_sequence< ::vascular::vascular_node > vascular_node_sequence;
    typedef vascular_node_sequence::iterator vascular_node_iterator;
    typedef vascular_node_sequence::const_iterator vascular_node_const_iterator;

    const vascular_node_sequence&
    vascular_node () const;

    vascular_node_sequence&
    vascular_node ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (list_of_vascular_nodes&) const;

    private:
    vascular_node_sequence vascular_node_;
    ::common::custom* custom_;
  };

  // boundary_node (variable-length)
  //
  class boundary_node
  {
    private:
    boundary_node (const boundary_node&);
    boundary_node& operator= (const boundary_node&);

    public:
    boundary_node ();

    boundary_node*
    _clone () const;

    ~boundary_node ();

    // node_ID
    //
    bool
    node_ID_present () const;

    void
    node_ID_present (bool);

    unsigned int
    node_ID () const;

    unsigned int&
    node_ID ();

    void
    node_ID (unsigned int);

    // fluid_flow_velocity
    //
    bool
    fluid_flow_velocity_present () const;

    const ::common::units_decimal&
    fluid_flow_velocity () const;

    ::common::units_decimal&
    fluid_flow_velocity ();

    void
    fluid_flow_velocity (::common::units_decimal*);

    ::common::units_decimal*
    fluid_flow_velocity_detach ();

    // variables
    //
    bool
    variables_present () const;

    const ::variables::list_of_variables&
    variables () const;

    ::variables::list_of_variables&
    variables ();

    void
    variables (::variables::list_of_variables*);

    ::variables::list_of_variables*
    variables_detach ();

    // boundary_conditions
    //
    bool
    boundary_conditions_present () const;

    const ::vascular::boundary_conditions&
    boundary_conditions () const;

    ::vascular::boundary_conditions&
    boundary_conditions ();

    void
    boundary_conditions (::vascular::boundary_conditions*);

    ::vascular::boundary_conditions*
    boundary_conditions_detach ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (boundary_node&) const;

    private:
    unsigned int node_ID_;
    unsigned char node_ID_present_;
    ::common::units_decimal* fluid_flow_velocity_;
    ::variables::list_of_variables* variables_;
    ::vascular::boundary_conditions* boundary_conditions_;
    ::common::custom* custom_;
  };

  // list_of_boundary_nodes (variable-length)
  //
  class list_of_boundary_nodes
  {
    private:
    list_of_boundary_nodes (const list_of_boundary_nodes&);
    list_of_boundary_nodes& operator= (const list_of_boundary_nodes&);

    public:
    list_of_boundary_nodes ();

    list_of_boundary_nodes*
    _clone () const;

    ~list_of_boundary_nodes ();

    // boundary_node
    //
    typedef ::xsde::cxx::hybrid::var_sequence< ::vascular::boundary_node > boundary_node_sequence;
    typedef boundary_node_sequence::iterator boundary_node_iterator;
    typedef boundary_node_sequence::const_iterator boundary_node_const_iterator;

    const boundary_node_sequence&
    boundary_node () const;

    boundary_node_sequence&
    boundary_node ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (list_of_boundary_nodes&) const;

    private:
    boundary_node_sequence boundary_node_;
    ::common::custom* custom_;
  };

  // boundary_conditions (variable-length)
  //
  class boundary_conditions
  {
    private:
    boundary_conditions (const boundary_conditions&);
    boundary_conditions& operator= (const boundary_conditions&);

    public:
    boundary_conditions ();

    boundary_conditions*
    _clone () const;

    ~boundary_conditions ();

    // ID
    //
    bool
    ID_present () const;

    void
    ID_present (bool);

    unsigned int
    ID () const;

    unsigned int&
    ID ();

    void
    ID (unsigned int);

    // boundary_condition
    //
    typedef ::xsde::cxx::hybrid::var_sequence< ::vascular::boundary_condition > boundary_condition_sequence;
    typedef boundary_condition_sequence::iterator boundary_condition_iterator;
    typedef boundary_condition_sequence::const_iterator boundary_condition_const_iterator;

    const boundary_condition_sequence&
    boundary_condition () const;

    boundary_condition_sequence&
    boundary_condition ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (boundary_conditions&) const;

    private:
    unsigned int ID_;
    unsigned char ID_present_;
    boundary_condition_sequence boundary_condition_;
    ::common::custom* custom_;
  };

  // boundary_type (fixed-length)
  //
  class boundary_type
  {
    public:
    enum value_type
    {
      Neumann,
      Dirichlet,
      Periodic,
      Anti_Periodic,
      Reflecting,
      Anti_Reflecting
    };

    boundary_type ();
    boundary_type (value_type);

    void
    value (value_type);

    operator value_type () const
    {
      return value_;
    }

    const char*
    string () const;

    private:
    value_type value_;
  };

  // boundary_condition (variable-length)
  //
  class boundary_condition
  {
    private:
    boundary_condition (const boundary_condition&);
    boundary_condition& operator= (const boundary_condition&);

    public:
    boundary_condition ();

    boundary_condition*
    _clone () const;

    ~boundary_condition ();

    // ID
    //
    bool
    ID_present () const;

    void
    ID_present (bool);

    unsigned int
    ID () const;

    unsigned int&
    ID ();

    void
    ID (unsigned int);

    // variable_ID
    //
    unsigned int
    variable_ID () const;

    unsigned int&
    variable_ID ();

    void
    variable_ID (unsigned int);

    // boundary_type
    //
    const ::vascular::boundary_type&
    boundary_type () const;

    ::vascular::boundary_type&
    boundary_type ();

    void
    boundary_type (const ::vascular::boundary_type&);

    // value
    //
    bool
    value_present () const;

    const ::common::units_decimal&
    value () const;

    ::common::units_decimal&
    value ();

    void
    value (::common::units_decimal*);

    ::common::units_decimal*
    value_detach ();

    // direction
    //
    bool
    direction_present () const;

    void
    direction_present (bool);

    const ::std::string&
    direction () const;

    ::std::string&
    direction ();

    void
    direction (const ::std::string&);

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (boundary_condition&) const;

    private:
    unsigned int ID_;
    unsigned char ID_present_;
    unsigned int variable_ID_;
    ::vascular::boundary_type boundary_type_;
    ::common::units_decimal* value_;
    ::std::string direction_;
    unsigned char direction_present_;
    ::common::custom* custom_;
  };

  // vascular_segments (variable-length)
  //
  class vascular_segments
  {
    private:
    vascular_segments (const vascular_segments&);
    vascular_segments& operator= (const vascular_segments&);

    public:
    vascular_segments ();

    vascular_segments*
    _clone () const;

    ~vascular_segments ();

    // vascular_segment
    //
    typedef ::xsde::cxx::hybrid::var_sequence< ::vascular::vascular_segment > vascular_segment_sequence;
    typedef vascular_segment_sequence::iterator vascular_segment_iterator;
    typedef vascular_segment_sequence::const_iterator vascular_segment_const_iterator;

    const vascular_segment_sequence&
    vascular_segment () const;

    vascular_segment_sequence&
    vascular_segment ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (vascular_segments&) const;

    private:
    vascular_segment_sequence vascular_segment_;
    ::common::custom* custom_;
  };

  // vascular_segment (variable-length)
  //
  class vascular_segment
  {
    private:
    vascular_segment (const vascular_segment&);
    vascular_segment& operator= (const vascular_segment&);

    public:
    vascular_segment ();

    vascular_segment*
    _clone () const;

    ~vascular_segment ();

    // endpoint_1
    //
    const ::vascular::endpoint&
    endpoint_1 () const;

    ::vascular::endpoint&
    endpoint_1 ();

    void
    endpoint_1 (::vascular::endpoint*);

    ::vascular::endpoint*
    endpoint_1_detach ();

    // endpoint_2
    //
    const ::vascular::endpoint&
    endpoint_2 () const;

    ::vascular::endpoint&
    endpoint_2 ();

    void
    endpoint_2 (::vascular::endpoint*);

    ::vascular::endpoint*
    endpoint_2_detach ();

    // surface
    //
    bool
    surface_present () const;

    const ::vascular::surface_properties&
    surface () const;

    ::vascular::surface_properties&
    surface ();

    void
    surface (::vascular::surface_properties*);

    ::vascular::surface_properties*
    surface_detach ();

    // interior
    //
    bool
    interior_present () const;

    const ::vascular::volume_properties&
    interior () const;

    ::vascular::volume_properties&
    interior ();

    void
    interior (::vascular::volume_properties*);

    ::vascular::volume_properties*
    interior_detach ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (vascular_segment&) const;

    private:
    ::vascular::endpoint* endpoint_1_;
    ::vascular::endpoint* endpoint_2_;
    ::vascular::surface_properties* surface_;
    ::vascular::volume_properties* interior_;
    ::common::custom* custom_;
  };

  // endpoint (variable-length)
  //
  class endpoint
  {
    private:
    endpoint (const endpoint&);
    endpoint& operator= (const endpoint&);

    public:
    endpoint ();

    endpoint*
    _clone () const;

    ~endpoint ();

    // node_ID
    //
    bool
    node_ID_present () const;

    void
    node_ID_present (bool);

    unsigned int
    node_ID () const;

    unsigned int&
    node_ID ();

    void
    node_ID (unsigned int);

    // lengths
    //
    bool
    lengths_present () const;

    const ::phenotype_common::lengths&
    lengths () const;

    ::phenotype_common::lengths&
    lengths ();

    void
    lengths (::phenotype_common::lengths*);

    ::phenotype_common::lengths*
    lengths_detach ();

    // areas
    //
    bool
    areas_present () const;

    const ::phenotype_common::areas_2D&
    areas () const;

    ::phenotype_common::areas_2D&
    areas ();

    void
    areas (::phenotype_common::areas_2D*);

    ::phenotype_common::areas_2D*
    areas_detach ();

    // fluid_flow_velocity
    //
    bool
    fluid_flow_velocity_present () const;

    const ::common::units_decimal&
    fluid_flow_velocity () const;

    ::common::units_decimal&
    fluid_flow_velocity ();

    void
    fluid_flow_velocity (::common::units_decimal*);

    ::common::units_decimal*
    fluid_flow_velocity_detach ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (endpoint&) const;

    private:
    unsigned int node_ID_;
    unsigned char node_ID_present_;
    ::phenotype_common::lengths* lengths_;
    ::phenotype_common::areas_2D* areas_;
    ::common::units_decimal* fluid_flow_velocity_;
    ::common::custom* custom_;
  };

  // surface_properties (variable-length)
  //
  class surface_properties
  {
    private:
    surface_properties (const surface_properties&);
    surface_properties& operator= (const surface_properties&);

    public:
    surface_properties ();

    surface_properties*
    _clone () const;

    ~surface_properties ();

    // areas
    //
    bool
    areas_present () const;

    const ::phenotype_common::areas_3D&
    areas () const;

    ::phenotype_common::areas_3D&
    areas ();

    void
    areas (::phenotype_common::areas_3D*);

    ::phenotype_common::areas_3D*
    areas_detach ();

    // fluid_flow_velocity
    //
    bool
    fluid_flow_velocity_present () const;

    const ::common::units_decimal&
    fluid_flow_velocity () const;

    ::common::units_decimal&
    fluid_flow_velocity ();

    void
    fluid_flow_velocity (::common::units_decimal*);

    ::common::units_decimal*
    fluid_flow_velocity_detach ();

    // mechanics
    //
    bool
    mechanics_present () const;

    const ::phenotype_common::mechanics&
    mechanics () const;

    ::phenotype_common::mechanics&
    mechanics ();

    void
    mechanics (::phenotype_common::mechanics*);

    ::phenotype_common::mechanics*
    mechanics_detach ();

    // permeability
    //
    bool
    permeability_present () const;

    const ::common::units_decimal&
    permeability () const;

    ::common::units_decimal&
    permeability ();

    void
    permeability (::common::units_decimal*);

    ::common::units_decimal*
    permeability_detach ();

    // surface_proteins
    //
    bool
    surface_proteins_present () const;

    const ::variables::list_of_variables&
    surface_proteins () const;

    ::variables::list_of_variables&
    surface_proteins ();

    void
    surface_proteins (::variables::list_of_variables*);

    ::variables::list_of_variables*
    surface_proteins_detach ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (surface_properties&) const;

    private:
    ::phenotype_common::areas_3D* areas_;
    ::common::units_decimal* fluid_flow_velocity_;
    ::phenotype_common::mechanics* mechanics_;
    ::common::units_decimal* permeability_;
    ::variables::list_of_variables* surface_proteins_;
    ::common::custom* custom_;
  };

  // volume_properties (variable-length)
  //
  class volume_properties
  {
    private:
    volume_properties (const volume_properties&);
    volume_properties& operator= (const volume_properties&);

    public:
    volume_properties ();

    volume_properties*
    _clone () const;

    ~volume_properties ();

    // fluid_flow_velocity
    //
    bool
    fluid_flow_velocity_present () const;

    const ::common::units_decimal&
    fluid_flow_velocity () const;

    ::common::units_decimal&
    fluid_flow_velocity ();

    void
    fluid_flow_velocity (::common::units_decimal*);

    ::common::units_decimal*
    fluid_flow_velocity_detach ();

    // variables
    //
    bool
    variables_present () const;

    const ::variables::list_of_variables&
    variables () const;

    ::variables::list_of_variables&
    variables ();

    void
    variables (::variables::list_of_variables*);

    ::variables::list_of_variables*
    variables_detach ();

    // volumes
    //
    bool
    volumes_present () const;

    const ::phenotype_common::volumes&
    volumes () const;

    ::phenotype_common::volumes&
    volumes ();

    void
    volumes (::phenotype_common::volumes*);

    ::phenotype_common::volumes*
    volumes_detach ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (volume_properties&) const;

    private:
    ::common::units_decimal* fluid_flow_velocity_;
    ::variables::list_of_variables* variables_;
    ::phenotype_common::volumes* volumes_;
    ::common::custom* custom_;
  };

  // vascular_network (variable-length)
  //
  class vascular_network
  {
    private:
    vascular_network (const vascular_network&);
    vascular_network& operator= (const vascular_network&);

    public:
    vascular_network ();

    vascular_network*
    _clone () const;

    ~vascular_network ();

    // ID
    //
    bool
    ID_present () const;

    void
    ID_present (bool);

    unsigned int
    ID () const;

    unsigned int&
    ID ();

    void
    ID (unsigned int);

    // keywords
    //
    bool
    keywords_present () const;

    void
    keywords_present (bool);

    const ::std::string&
    keywords () const;

    ::std::string&
    keywords ();

    void
    keywords (const ::std::string&);

    // name
    //
    bool
    name_present () const;

    void
    name_present (bool);

    const ::std::string&
    name () const;

    ::std::string&
    name ();

    void
    name (const ::std::string&);

    // vascular_nodes
    //
    bool
    vascular_nodes_present () const;

    const ::vascular::list_of_vascular_nodes&
    vascular_nodes () const;

    ::vascular::list_of_vascular_nodes&
    vascular_nodes ();

    void
    vascular_nodes (::vascular::list_of_vascular_nodes*);

    ::vascular::list_of_vascular_nodes*
    vascular_nodes_detach ();

    // boundary_nodes
    //
    bool
    boundary_nodes_present () const;

    const ::vascular::list_of_boundary_nodes&
    boundary_nodes () const;

    ::vascular::list_of_boundary_nodes&
    boundary_nodes ();

    void
    boundary_nodes (::vascular::list_of_boundary_nodes*);

    ::vascular::list_of_boundary_nodes*
    boundary_nodes_detach ();

    // vascular_segments
    //
    bool
    vascular_segments_present () const;

    const ::vascular::vascular_segments&
    vascular_segments () const;

    ::vascular::vascular_segments&
    vascular_segments ();

    void
    vascular_segments (::vascular::vascular_segments*);

    ::vascular::vascular_segments*
    vascular_segments_detach ();

    // voxels
    //
    bool
    voxels_present () const;

    const ::mesh::int_list_xpath&
    voxels () const;

    ::mesh::int_list_xpath&
    voxels ();

    void
    voxels (::mesh::int_list_xpath*);

    ::mesh::int_list_xpath*
    voxels_detach ();

    // custom
    //
    bool
    custom_present () const;

    const ::common::custom&
    custom () const;

    ::common::custom&
    custom ();

    void
    custom (::common::custom*);

    ::common::custom*
    custom_detach ();

    void
    _copy (vascular_network&) const;

    private:
    unsigned int ID_;
    unsigned char ID_present_;
    ::std::string keywords_;
    unsigned char keywords_present_;
    ::std::string name_;
    unsigned char name_present_;
    ::vascular::list_of_vascular_nodes* vascular_nodes_;
    ::vascular::list_of_boundary_nodes* boundary_nodes_;
    ::vascular::vascular_segments* vascular_segments_;
    ::mesh::int_list_xpath* voxels_;
    ::common::custom* custom_;
  };
}

#ifndef XSDE_DONT_INCLUDE_INLINE

#include "common.ipp"

#include "mesh.ipp"

#include "variables.ipp"

#include "phenotype_common.ipp"

#endif // XSDE_DONT_INCLUDE_INLINE

#ifndef XSDE_DONT_INCLUDE_INLINE
#include "vascular.ipp"
#endif // XSDE_DONT_INCLUDE_INLINE

// Begin epilogue.
//
//
// End epilogue.

#include <xsde/cxx/post.hxx>

#endif // VASCULAR_HPP
