// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

#include "phenotype_dataset-sskel.hpp"

#include <assert.h>

#include <string.h>
#include <xsde/cxx/serializer/substitution-map.hxx>
#include <xsde/cxx/serializer/validating/inheritance-map.hxx>

static
const ::xsde::cxx::serializer::substitution_map_init
_xsde_substitution_map_init_;

static
const ::xsde::cxx::serializer::validating::inheritance_map_init
_xsde_inheritance_map_init_;

namespace phenotype_dataset
{
  // phenotype_dataset_sskel
  //

  bool phenotype_dataset_sskel::
  keywords_present ()
  {
    return this->phenotype_dataset_impl_ ? this->phenotype_dataset_impl_->keywords_present () : false;
  }

  bool phenotype_dataset_sskel::
  ID_present ()
  {
    return this->phenotype_dataset_impl_ ? this->phenotype_dataset_impl_->ID_present () : false;
  }

  bool phenotype_dataset_sskel::
  microenvironment_present ()
  {
    return this->phenotype_dataset_impl_ ? this->phenotype_dataset_impl_->microenvironment_present () : false;
  }

  bool phenotype_dataset_sskel::
  phenotype_next ()
  {
    return this->phenotype_dataset_impl_ ? this->phenotype_dataset_impl_->phenotype_next () : false;
  }

  bool phenotype_dataset_sskel::
  cell_part_next ()
  {
    return this->phenotype_dataset_impl_ ? this->phenotype_dataset_impl_->cell_part_next () : false;
  }

  bool phenotype_dataset_sskel::
  custom_present ()
  {
    return this->phenotype_dataset_impl_ ? this->phenotype_dataset_impl_->custom_present () : false;
  }

  void phenotype_dataset_sskel::
  _reset ()
  {
    if (this->resetting_)
      return;

    typedef ::xsde::cxx::serializer::validating::complex_content base;
    base::_reset ();

    if (this->keywords_serializer_)
      this->keywords_serializer_->_reset ();

    if (this->ID_serializer_)
      this->ID_serializer_->_reset ();

    this->resetting_ = true;

    if (this->microenvironment_serializer_)
      this->microenvironment_serializer_->_reset ();

    if (this->microenvironment_serializer_map_)
      this->microenvironment_serializer_map_->reset ();

    if (this->phenotype_serializer_)
      this->phenotype_serializer_->_reset ();

    if (this->phenotype_serializer_map_)
      this->phenotype_serializer_map_->reset ();

    if (this->cell_part_serializer_)
      this->cell_part_serializer_->_reset ();

    if (this->cell_part_serializer_map_)
      this->cell_part_serializer_map_->reset ();

    if (this->custom_serializer_)
      this->custom_serializer_->_reset ();

    if (this->custom_serializer_map_)
      this->custom_serializer_map_->reset ();

    this->resetting_ = false;
  }

  const char* phenotype_dataset_sskel::
  _static_type ()
  {
    return "phenotype_dataset phenotype_dataset";
  }

  const char* phenotype_dataset_sskel::
  _dynamic_type () const
  {
    return _static_type ();
  }
}

namespace phenotype_dataset
{
  // Element validation and serialization for phenotype_dataset_sskel.
  //
  void phenotype_dataset_sskel::
  _serialize_content ()
  {
    ::xsde::cxx::serializer::context& ctx = this->_context ();

    // microenvironment
    //
    if (this->microenvironment_present ())
    {
      ctx.type_id (0);
      const ::microenvironment::microenvironment& r = this->microenvironment ();

      const void* t = ctx.type_id ();
      const char* dt = 0;
      ::microenvironment::microenvironment_sskel* s = 0;

      if (t == 0 && this->microenvironment_serializer_ != 0)
        s = this->microenvironment_serializer_;
      else if (this->microenvironment_serializer_map_ != 0)
      {
        ::xml_schema::serializer_base* b = this->microenvironment_serializer_map_->find (t);

        if (b != 0)
        {
          dt = b->_dynamic_type ();
          const char* st = ::microenvironment::microenvironment_sskel::_static_type ();

          if (strcmp (dt, st) == 0)
            dt = 0;

          if (dt != 0 && !::xsde::cxx::serializer::validating::inheritance_map_instance ().check (dt, st))
          {
            ctx.schema_error (::xsde::cxx::schema_error::not_derived);
            return;
          }

          s = static_cast< ::microenvironment::microenvironment_sskel* > (b);
        }
      }

      if (s)
      {
        s->pre (r);
        this->_start_element ("microenvironment");
        if (dt != 0)
          this->_set_type (dt);

        s->_pre_impl (ctx);

        if (ctx.error_type ())
          return;

        s->_serialize_attributes ();

        if (ctx.error_type ())
          return;

        s->_serialize_content ();

        if (ctx.error_type ())
          return;

        s->_post_impl ();

        if (ctx.error_type ())
          return;

        this->_end_element ();
        s->post ();
      }
    }

    // phenotype
    //
    {
      size_t i = 0;
      for (; i < 3UL && this->phenotype_next (); ++i)
      {
        ctx.type_id (0);
        const ::phenotype::phenotype& r = this->phenotype ();

        const void* t = ctx.type_id ();
        const char* dt = 0;
        ::phenotype::phenotype_sskel* s = 0;

        if (t == 0 && this->phenotype_serializer_ != 0)
          s = this->phenotype_serializer_;
        else if (this->phenotype_serializer_map_ != 0)
        {
          ::xml_schema::serializer_base* b = this->phenotype_serializer_map_->find (t);

          if (b != 0)
          {
            dt = b->_dynamic_type ();
            const char* st = ::phenotype::phenotype_sskel::_static_type ();

            if (strcmp (dt, st) == 0)
              dt = 0;

            if (dt != 0 && !::xsde::cxx::serializer::validating::inheritance_map_instance ().check (dt, st))
            {
              ctx.schema_error (::xsde::cxx::schema_error::not_derived);
              return;
            }

            s = static_cast< ::phenotype::phenotype_sskel* > (b);
          }
        }

        if (s)
        {
          s->pre (r);
          this->_start_element ("phenotype");
          if (dt != 0)
            this->_set_type (dt);

          s->_pre_impl (ctx);

          if (ctx.error_type ())
            return;

          s->_serialize_attributes ();

          if (ctx.error_type ())
            return;

          s->_serialize_content ();

          if (ctx.error_type ())
            return;

          s->_post_impl ();

          if (ctx.error_type ())
            return;

          this->_end_element ();
          s->post ();
        }
      }
    }

    // cell_part
    //
    while (this->cell_part_next ())
    {
      ctx.type_id (0);
      const ::phenotype_base::cell_parts& r = this->cell_part ();

      const void* t = ctx.type_id ();
      const char* dt = 0;
      ::phenotype_base::cell_parts_sskel* s = 0;

      if (t == 0 && this->cell_part_serializer_ != 0)
        s = this->cell_part_serializer_;
      else if (this->cell_part_serializer_map_ != 0)
      {
        ::xml_schema::serializer_base* b = this->cell_part_serializer_map_->find (t);

        if (b != 0)
        {
          dt = b->_dynamic_type ();
          const char* st = ::phenotype_base::cell_parts_sskel::_static_type ();

          if (strcmp (dt, st) == 0)
            dt = 0;

          if (dt != 0 && !::xsde::cxx::serializer::validating::inheritance_map_instance ().check (dt, st))
          {
            ctx.schema_error (::xsde::cxx::schema_error::not_derived);
            return;
          }

          s = static_cast< ::phenotype_base::cell_parts_sskel* > (b);
        }
      }

      if (s)
      {
        s->pre (r);
        this->_start_element ("cell_part");
        if (dt != 0)
          this->_set_type (dt);

        s->_pre_impl (ctx);

        if (ctx.error_type ())
          return;

        s->_serialize_attributes ();

        if (ctx.error_type ())
          return;

        s->_serialize_content ();

        if (ctx.error_type ())
          return;

        s->_post_impl ();

        if (ctx.error_type ())
          return;

        this->_end_element ();
        s->post ();
      }
    }

    // custom
    //
    if (this->custom_present ())
    {
      ctx.type_id (0);
      const ::common::custom& r = this->custom ();

      const void* t = ctx.type_id ();
      const char* dt = 0;
      ::common::custom_sskel* s = 0;

      if (t == 0 && this->custom_serializer_ != 0)
        s = this->custom_serializer_;
      else if (this->custom_serializer_map_ != 0)
      {
        ::xml_schema::serializer_base* b = this->custom_serializer_map_->find (t);

        if (b != 0)
        {
          dt = b->_dynamic_type ();
          const char* st = ::common::custom_sskel::_static_type ();

          if (strcmp (dt, st) == 0)
            dt = 0;

          if (dt != 0 && !::xsde::cxx::serializer::validating::inheritance_map_instance ().check (dt, st))
          {
            ctx.schema_error (::xsde::cxx::schema_error::not_derived);
            return;
          }

          s = static_cast< ::common::custom_sskel* > (b);
        }
      }

      if (s)
      {
        s->pre (r);
        this->_start_element ("custom");
        if (dt != 0)
          this->_set_type (dt);

        s->_pre_impl (ctx);

        if (ctx.error_type ())
          return;

        s->_serialize_attributes ();

        if (ctx.error_type ())
          return;

        s->_serialize_content ();

        if (ctx.error_type ())
          return;

        s->_post_impl ();

        if (ctx.error_type ())
          return;

        this->_end_element ();
        s->post ();
      }
    }
  }
}

namespace phenotype_dataset
{
  // Attribute validation and serialization for phenotype_dataset_sskel.
  //
  void phenotype_dataset_sskel::
  _serialize_attributes ()
  {
    ::xsde::cxx::serializer::context& ctx = this->_context ();

    // keywords
    //
    if (this->keywords_present ())
    {
      const ::std::string& r = this->keywords ();

      if (this->keywords_serializer_)
      {
        this->keywords_serializer_->pre (r);
        this->_start_attribute ("keywords");
        this->keywords_serializer_->_pre_impl (ctx);

        if (ctx.error_type ())
          return;

        this->keywords_serializer_->_serialize_content ();

        if (ctx.error_type ())
          return;

        this->keywords_serializer_->_post_impl ();

        if (ctx.error_type ())
          return;

        this->_end_attribute ();
        this->keywords_serializer_->post ();
      }
    }

    // ID
    //
    if (this->ID_present ())
    {
      unsigned long long r = this->ID ();

      if (this->ID_serializer_)
      {
        this->ID_serializer_->pre (r);
        this->_start_attribute ("ID");
        this->ID_serializer_->_pre_impl (ctx);

        if (ctx.error_type ())
          return;

        this->ID_serializer_->_serialize_content ();

        if (ctx.error_type ())
          return;

        this->ID_serializer_->_post_impl ();

        if (ctx.error_type ())
          return;

        this->_end_attribute ();
        this->ID_serializer_->post ();
      }
    }
  }
}

// Begin epilogue.
//
//
// End epilogue.

