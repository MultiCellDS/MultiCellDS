// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

// Begin prologue.
//
//
// End prologue.

namespace cell
{
  // population_definition_pskel
  //

  inline
  void population_definition_pskel::
  ID_parser (::xml_schema::unsigned_int_pskel& p)
  {
    this->ID_parser_ = &p;
  }

  inline
  void population_definition_pskel::
  name_parser (::xml_schema::string_pskel& p)
  {
    this->name_parser_ = &p;
  }

  inline
  void population_definition_pskel::
  units_parser (::xml_schema::string_pskel& p)
  {
    this->units_parser_ = &p;
  }

  inline
  void population_definition_pskel::
  phenotype_dataset_parser (::phenotype_dataset::phenotype_dataset_pskel& p)
  {
    this->phenotype_dataset_parser_ = &p;
  }

  inline
  void population_definition_pskel::
  phenotype_dataset_parser (::xml_schema::parser_map& m)
  {
    this->phenotype_dataset_parser_map_ = &m;
  }

  inline
  void population_definition_pskel::
  custom_parser (::common::custom_pskel& p)
  {
    this->custom_parser_ = &p;
  }

  inline
  void population_definition_pskel::
  custom_parser (::xml_schema::parser_map& m)
  {
    this->custom_parser_map_ = &m;
  }

  inline
  void population_definition_pskel::
  parsers (::xml_schema::unsigned_int_pskel& ID,
           ::xml_schema::string_pskel& name,
           ::xml_schema::string_pskel& units,
           ::phenotype_dataset::phenotype_dataset_pskel& phenotype_dataset,
           ::common::custom_pskel& custom)
  {
    this->ID_parser_ = &ID;
    this->name_parser_ = &name;
    this->units_parser_ = &units;
    this->phenotype_dataset_parser_ = &phenotype_dataset;
    this->custom_parser_ = &custom;
  }

  inline
  void population_definition_pskel::
  parser_maps (::xml_schema::parser_map& phenotype_dataset,
               ::xml_schema::parser_map& custom)
  {
    this->phenotype_dataset_parser_map_ = &phenotype_dataset;
    this->custom_parser_map_ = &custom;
  }

  inline
  population_definition_pskel::
  population_definition_pskel ()
  : population_definition_impl_ (0),
    ID_parser_ (0),
    name_parser_ (0),
    units_parser_ (0),
    phenotype_dataset_parser_ (0),
    phenotype_dataset_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  inline
  population_definition_pskel::
  population_definition_pskel (population_definition_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    population_definition_impl_ (impl),
    ID_parser_ (0),
    name_parser_ (0),
    units_parser_ (0),
    phenotype_dataset_parser_ (0),
    phenotype_dataset_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  // population_definitions_pskel
  //

  inline
  void population_definitions_pskel::
  population_definition_parser (::cell::population_definition_pskel& p)
  {
    this->population_definition_parser_ = &p;
  }

  inline
  void population_definitions_pskel::
  population_definition_parser (::xml_schema::parser_map& m)
  {
    this->population_definition_parser_map_ = &m;
  }

  inline
  void population_definitions_pskel::
  custom_parser (::common::custom_pskel& p)
  {
    this->custom_parser_ = &p;
  }

  inline
  void population_definitions_pskel::
  custom_parser (::xml_schema::parser_map& m)
  {
    this->custom_parser_map_ = &m;
  }

  inline
  void population_definitions_pskel::
  parsers (::cell::population_definition_pskel& population_definition,
           ::common::custom_pskel& custom)
  {
    this->population_definition_parser_ = &population_definition;
    this->custom_parser_ = &custom;
  }

  inline
  void population_definitions_pskel::
  parser_maps (::xml_schema::parser_map& population_definition,
               ::xml_schema::parser_map& custom)
  {
    this->population_definition_parser_map_ = &population_definition;
    this->custom_parser_map_ = &custom;
  }

  inline
  population_definitions_pskel::
  population_definitions_pskel ()
  : population_definitions_impl_ (0),
    population_definition_parser_ (0),
    population_definition_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  inline
  population_definitions_pskel::
  population_definitions_pskel (population_definitions_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    population_definitions_impl_ (impl),
    population_definition_parser_ (0),
    population_definition_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  // cell_pskel
  //

  inline
  void cell_pskel::
  ID_parser (::xml_schema::unsigned_int_pskel& p)
  {
    this->ID_parser_ = &p;
  }

  inline
  void cell_pskel::
  phenotype_dataset_parser (::phenotype_dataset::phenotype_dataset_pskel& p)
  {
    this->phenotype_dataset_parser_ = &p;
  }

  inline
  void cell_pskel::
  phenotype_dataset_parser (::xml_schema::parser_map& m)
  {
    this->phenotype_dataset_parser_map_ = &m;
  }

  inline
  void cell_pskel::
  state_parser (::state::state_pskel& p)
  {
    this->state_parser_ = &p;
  }

  inline
  void cell_pskel::
  state_parser (::xml_schema::parser_map& m)
  {
    this->state_parser_map_ = &m;
  }

  inline
  void cell_pskel::
  custom_parser (::common::custom_pskel& p)
  {
    this->custom_parser_ = &p;
  }

  inline
  void cell_pskel::
  custom_parser (::xml_schema::parser_map& m)
  {
    this->custom_parser_map_ = &m;
  }

  inline
  void cell_pskel::
  parsers (::xml_schema::unsigned_int_pskel& ID,
           ::phenotype_dataset::phenotype_dataset_pskel& phenotype_dataset,
           ::state::state_pskel& state,
           ::common::custom_pskel& custom)
  {
    this->ID_parser_ = &ID;
    this->phenotype_dataset_parser_ = &phenotype_dataset;
    this->state_parser_ = &state;
    this->custom_parser_ = &custom;
  }

  inline
  void cell_pskel::
  parser_maps (::xml_schema::parser_map& phenotype_dataset,
               ::xml_schema::parser_map& state,
               ::xml_schema::parser_map& custom)
  {
    this->phenotype_dataset_parser_map_ = &phenotype_dataset;
    this->state_parser_map_ = &state;
    this->custom_parser_map_ = &custom;
  }

  inline
  cell_pskel::
  cell_pskel ()
  : cell_impl_ (0),
    ID_parser_ (0),
    phenotype_dataset_parser_ (0),
    phenotype_dataset_parser_map_ (0),
    state_parser_ (0),
    state_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  inline
  cell_pskel::
  cell_pskel (cell_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    cell_impl_ (impl),
    ID_parser_ (0),
    phenotype_dataset_parser_ (0),
    phenotype_dataset_parser_map_ (0),
    state_parser_ (0),
    state_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  // cell_population_individual_pskel
  //

  inline
  void cell_population_individual_pskel::
  type_parser (::xml_schema::string_pskel& p)
  {
    this->type_parser_ = &p;
  }

  inline
  void cell_population_individual_pskel::
  population_ID_parser (::xml_schema::unsigned_int_pskel& p)
  {
    this->population_ID_parser_ = &p;
  }

  inline
  void cell_population_individual_pskel::
  cell_parser (::cell::cell_pskel& p)
  {
    this->cell_parser_ = &p;
  }

  inline
  void cell_population_individual_pskel::
  cell_parser (::xml_schema::parser_map& m)
  {
    this->cell_parser_map_ = &m;
  }

  inline
  void cell_population_individual_pskel::
  custom_parser (::common::custom_pskel& p)
  {
    this->custom_parser_ = &p;
  }

  inline
  void cell_population_individual_pskel::
  custom_parser (::xml_schema::parser_map& m)
  {
    this->custom_parser_map_ = &m;
  }

  inline
  void cell_population_individual_pskel::
  parsers (::xml_schema::string_pskel& type,
           ::xml_schema::unsigned_int_pskel& population_ID,
           ::cell::cell_pskel& cell,
           ::common::custom_pskel& custom)
  {
    this->type_parser_ = &type;
    this->population_ID_parser_ = &population_ID;
    this->cell_parser_ = &cell;
    this->custom_parser_ = &custom;
  }

  inline
  void cell_population_individual_pskel::
  parser_maps (::xml_schema::parser_map& cell,
               ::xml_schema::parser_map& custom)
  {
    this->cell_parser_map_ = &cell;
    this->custom_parser_map_ = &custom;
  }

  inline
  cell_population_individual_pskel::
  cell_population_individual_pskel ()
  : cell_population_individual_impl_ (0),
    type_parser_ (0),
    population_ID_parser_ (0),
    cell_parser_ (0),
    cell_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  inline
  cell_population_individual_pskel::
  cell_population_individual_pskel (cell_population_individual_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    cell_population_individual_impl_ (impl),
    type_parser_ (0),
    population_ID_parser_ (0),
    cell_parser_ (0),
    cell_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  // cell_population_aggregate_pskel
  //

  inline
  void cell_population_aggregate_pskel::
  type_parser (::xml_schema::string_pskel& p)
  {
    this->type_parser_ = &p;
  }

  inline
  void cell_population_aggregate_pskel::
  population_ID_parser (::xml_schema::unsigned_int_pskel& p)
  {
    this->population_ID_parser_ = &p;
  }

  inline
  void cell_population_aggregate_pskel::
  value_parser (::common::units_decimal_pskel& p)
  {
    this->value_parser_ = &p;
  }

  inline
  void cell_population_aggregate_pskel::
  value_parser (::xml_schema::parser_map& m)
  {
    this->value_parser_map_ = &m;
  }

  inline
  void cell_population_aggregate_pskel::
  phenotype_dataset_parser (::phenotype_dataset::phenotype_dataset_pskel& p)
  {
    this->phenotype_dataset_parser_ = &p;
  }

  inline
  void cell_population_aggregate_pskel::
  phenotype_dataset_parser (::xml_schema::parser_map& m)
  {
    this->phenotype_dataset_parser_map_ = &m;
  }

  inline
  void cell_population_aggregate_pskel::
  state_parser (::state::state_pskel& p)
  {
    this->state_parser_ = &p;
  }

  inline
  void cell_population_aggregate_pskel::
  state_parser (::xml_schema::parser_map& m)
  {
    this->state_parser_map_ = &m;
  }

  inline
  void cell_population_aggregate_pskel::
  custom_parser (::common::custom_pskel& p)
  {
    this->custom_parser_ = &p;
  }

  inline
  void cell_population_aggregate_pskel::
  custom_parser (::xml_schema::parser_map& m)
  {
    this->custom_parser_map_ = &m;
  }

  inline
  void cell_population_aggregate_pskel::
  parsers (::xml_schema::string_pskel& type,
           ::xml_schema::unsigned_int_pskel& population_ID,
           ::common::units_decimal_pskel& value,
           ::phenotype_dataset::phenotype_dataset_pskel& phenotype_dataset,
           ::state::state_pskel& state,
           ::common::custom_pskel& custom)
  {
    this->type_parser_ = &type;
    this->population_ID_parser_ = &population_ID;
    this->value_parser_ = &value;
    this->phenotype_dataset_parser_ = &phenotype_dataset;
    this->state_parser_ = &state;
    this->custom_parser_ = &custom;
  }

  inline
  void cell_population_aggregate_pskel::
  parser_maps (::xml_schema::parser_map& value,
               ::xml_schema::parser_map& phenotype_dataset,
               ::xml_schema::parser_map& state,
               ::xml_schema::parser_map& custom)
  {
    this->value_parser_map_ = &value;
    this->phenotype_dataset_parser_map_ = &phenotype_dataset;
    this->state_parser_map_ = &state;
    this->custom_parser_map_ = &custom;
  }

  inline
  cell_population_aggregate_pskel::
  cell_population_aggregate_pskel ()
  : cell_population_aggregate_impl_ (0),
    type_parser_ (0),
    population_ID_parser_ (0),
    value_parser_ (0),
    value_parser_map_ (0),
    phenotype_dataset_parser_ (0),
    phenotype_dataset_parser_map_ (0),
    state_parser_ (0),
    state_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  inline
  cell_population_aggregate_pskel::
  cell_population_aggregate_pskel (cell_population_aggregate_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    cell_population_aggregate_impl_ (impl),
    type_parser_ (0),
    population_ID_parser_ (0),
    value_parser_ (0),
    value_parser_map_ (0),
    phenotype_dataset_parser_ (0),
    phenotype_dataset_parser_map_ (0),
    state_parser_ (0),
    state_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  // population_vector_pskel
  //

  inline
  void population_vector_pskel::
  voxel_ID_parser (::common::unsigned_int_list_pskel& p)
  {
    this->voxel_ID_parser_ = &p;
  }

  inline
  void population_vector_pskel::
  value_parser (::common::units_double_list_pskel& p)
  {
    this->value_parser_ = &p;
  }

  inline
  void population_vector_pskel::
  value_parser (::xml_schema::parser_map& m)
  {
    this->value_parser_map_ = &m;
  }

  inline
  void population_vector_pskel::
  cell_population_parser (::cell::cell_population_aggregate_pskel& p)
  {
    this->cell_population_parser_ = &p;
  }

  inline
  void population_vector_pskel::
  cell_population_parser (::xml_schema::parser_map& m)
  {
    this->cell_population_parser_map_ = &m;
  }

  inline
  void population_vector_pskel::
  custom_parser (::common::custom_pskel& p)
  {
    this->custom_parser_ = &p;
  }

  inline
  void population_vector_pskel::
  custom_parser (::xml_schema::parser_map& m)
  {
    this->custom_parser_map_ = &m;
  }

  inline
  void population_vector_pskel::
  parsers (::common::unsigned_int_list_pskel& voxel_ID,
           ::common::units_double_list_pskel& value,
           ::cell::cell_population_aggregate_pskel& cell_population,
           ::common::custom_pskel& custom)
  {
    this->voxel_ID_parser_ = &voxel_ID;
    this->value_parser_ = &value;
    this->cell_population_parser_ = &cell_population;
    this->custom_parser_ = &custom;
  }

  inline
  void population_vector_pskel::
  parser_maps (::xml_schema::parser_map& value,
               ::xml_schema::parser_map& cell_population,
               ::xml_schema::parser_map& custom)
  {
    this->value_parser_map_ = &value;
    this->cell_population_parser_map_ = &cell_population;
    this->custom_parser_map_ = &custom;
  }

  inline
  population_vector_pskel::
  population_vector_pskel ()
  : population_vector_impl_ (0),
    voxel_ID_parser_ (0),
    value_parser_ (0),
    value_parser_map_ (0),
    cell_population_parser_ (0),
    cell_population_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  inline
  population_vector_pskel::
  population_vector_pskel (population_vector_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    population_vector_impl_ (impl),
    voxel_ID_parser_ (0),
    value_parser_ (0),
    value_parser_map_ (0),
    cell_population_parser_ (0),
    cell_population_parser_map_ (0),
    custom_parser_ (0),
    custom_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  // cell_populations_pskel
  //

  inline
  void cell_populations_pskel::
  population_vector_parser (::cell::population_vector_pskel& p)
  {
    this->population_vector_parser_ = &p;
  }

  inline
  void cell_populations_pskel::
  population_vector_parser (::xml_schema::parser_map& m)
  {
    this->population_vector_parser_map_ = &m;
  }

  inline
  void cell_populations_pskel::
  cell_population_parser (::cell::cell_population_individual_pskel& p)
  {
    this->cell_population_parser_ = &p;
  }

  inline
  void cell_populations_pskel::
  cell_population_parser (::xml_schema::parser_map& m)
  {
    this->cell_population_parser_map_ = &m;
  }

  inline
  void cell_populations_pskel::
  parsers (::cell::population_vector_pskel& population_vector,
           ::cell::cell_population_individual_pskel& cell_population)
  {
    this->population_vector_parser_ = &population_vector;
    this->cell_population_parser_ = &cell_population;
  }

  inline
  void cell_populations_pskel::
  parser_maps (::xml_schema::parser_map& population_vector,
               ::xml_schema::parser_map& cell_population)
  {
    this->population_vector_parser_map_ = &population_vector;
    this->cell_population_parser_map_ = &cell_population;
  }

  inline
  cell_populations_pskel::
  cell_populations_pskel ()
  : cell_populations_impl_ (0),
    population_vector_parser_ (0),
    population_vector_parser_map_ (0),
    cell_population_parser_ (0),
    cell_population_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  inline
  cell_populations_pskel::
  cell_populations_pskel (cell_populations_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    cell_populations_impl_ (impl),
    population_vector_parser_ (0),
    population_vector_parser_map_ (0),
    cell_population_parser_ (0),
    cell_population_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  // cellular_information_pskel
  //

  inline
  void cellular_information_pskel::
  DCLs_parser (::cell_line::DCLs_pskel& p)
  {
    this->DCLs_parser_ = &p;
  }

  inline
  void cellular_information_pskel::
  DCLs_parser (::xml_schema::parser_map& m)
  {
    this->DCLs_parser_map_ = &m;
  }

  inline
  void cellular_information_pskel::
  population_definitions_parser (::cell::population_definitions_pskel& p)
  {
    this->population_definitions_parser_ = &p;
  }

  inline
  void cellular_information_pskel::
  population_definitions_parser (::xml_schema::parser_map& m)
  {
    this->population_definitions_parser_map_ = &m;
  }

  inline
  void cellular_information_pskel::
  mesh_parser (::mesh::mesh_pskel& p)
  {
    this->mesh_parser_ = &p;
  }

  inline
  void cellular_information_pskel::
  mesh_parser (::xml_schema::parser_map& m)
  {
    this->mesh_parser_map_ = &m;
  }

  inline
  void cellular_information_pskel::
  cell_populations_parser (::cell::cell_populations_pskel& p)
  {
    this->cell_populations_parser_ = &p;
  }

  inline
  void cellular_information_pskel::
  cell_populations_parser (::xml_schema::parser_map& m)
  {
    this->cell_populations_parser_map_ = &m;
  }

  inline
  void cellular_information_pskel::
  parsers (::cell_line::DCLs_pskel& DCLs,
           ::cell::population_definitions_pskel& population_definitions,
           ::mesh::mesh_pskel& mesh,
           ::cell::cell_populations_pskel& cell_populations)
  {
    this->DCLs_parser_ = &DCLs;
    this->population_definitions_parser_ = &population_definitions;
    this->mesh_parser_ = &mesh;
    this->cell_populations_parser_ = &cell_populations;
  }

  inline
  void cellular_information_pskel::
  parser_maps (::xml_schema::parser_map& DCLs,
               ::xml_schema::parser_map& population_definitions,
               ::xml_schema::parser_map& mesh,
               ::xml_schema::parser_map& cell_populations)
  {
    this->DCLs_parser_map_ = &DCLs;
    this->population_definitions_parser_map_ = &population_definitions;
    this->mesh_parser_map_ = &mesh;
    this->cell_populations_parser_map_ = &cell_populations;
  }

  inline
  cellular_information_pskel::
  cellular_information_pskel ()
  : cellular_information_impl_ (0),
    DCLs_parser_ (0),
    DCLs_parser_map_ (0),
    population_definitions_parser_ (0),
    population_definitions_parser_map_ (0),
    mesh_parser_ (0),
    mesh_parser_map_ (0),
    cell_populations_parser_ (0),
    cell_populations_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_),
    v_all_count_ (4UL, v_all_first_)
  {
  }

  inline
  cellular_information_pskel::
  cellular_information_pskel (cellular_information_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    cellular_information_impl_ (impl),
    DCLs_parser_ (0),
    DCLs_parser_map_ (0),
    population_definitions_parser_ (0),
    population_definitions_parser_map_ (0),
    mesh_parser_ (0),
    mesh_parser_map_ (0),
    cell_populations_parser_ (0),
    cell_populations_parser_map_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_),
    v_all_count_ (4UL, v_all_first_)
  {
  }
}

// Begin epilogue.
//
//
// End epilogue.

