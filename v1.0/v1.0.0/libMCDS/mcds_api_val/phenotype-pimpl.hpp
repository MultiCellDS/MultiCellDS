// Copyright (c) 2005-2016 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
// Furthermore, Code Synthesis Tools CC makes a special exception for
// the Free/Libre and Open Source Software (FLOSS) which is described
// in the accompanying FLOSSE file.
//

#ifndef PHENOTYPE_PIMPL_HPP
#define PHENOTYPE_PIMPL_HPP

#include <xsde/cxx/pre.hxx>

// Begin prologue.
//
//
// End prologue.

#ifndef XSDE_OMIT_PAGGR
#  define XSDE_OMIT_PAGGR
#  define PHENOTYPE_PIMPL_HPP_CLEAR_OMIT_PAGGR
#endif

#include "phenotype-pskel.hpp"

#include <xsde/cxx/stack.hxx>

#include "common-pimpl.hpp"

#include "cell_cycle-pimpl.hpp"

#include "phenotype_base-pimpl.hpp"

namespace phenotype
{
  class phenotype_elements_pimpl: public phenotype_elements_pskel
  {
    public:
    phenotype_elements_pimpl (bool = false);

    ~phenotype_elements_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Elements.
    //
    virtual void
    adhesion (::phenotype_common::adhesion*);

    virtual void
    geometrical_properties (::phenotype_common::geometrical_properties*);

    virtual void
    mass (::phenotype_common::mass*);

    virtual void
    mechanics (::phenotype_common::mechanics*);

    virtual void
    motility (::phenotype_common::motility*);

    virtual void
    PKPD (::pkpd::PKPD*);

    virtual void
    timescale (::phenotype_base::expected_timescale*);

    virtual void
    transport_processes (::phenotype_common::transport_processes*);

    virtual void
    custom (::common::custom*);

    virtual ::phenotype::phenotype_elements*
    post_phenotype_elements ();

    public:
    void
    pre_impl (::phenotype::phenotype_elements*);

    public:
    ::cell_cycle::cycles_and_deaths_pimpl base_impl_;

    public:
    struct phenotype_elements_pimpl_state
    {
      ::phenotype::phenotype_elements* phenotype_elements_;
    };

    phenotype_elements_pimpl_state phenotype_elements_pimpl_state_;
    bool phenotype_elements_pimpl_base_;
  };

  class phenotype_pimpl: public phenotype_pskel
  {
    public:
    phenotype_pimpl (bool = false);

    ~phenotype_pimpl ();

    virtual void
    _reset ();

    virtual void
    pre ();

    // Attributes.
    //
    virtual void
    type (const ::phenotype_base::phenotype_type&);

    virtual ::phenotype::phenotype*
    post_phenotype ();

    public:
    void
    pre_impl (::phenotype::phenotype*);

    public:
    ::phenotype::phenotype_elements_pimpl base_impl_;

    public:
    struct phenotype_pimpl_state
    {
      ::phenotype::phenotype* phenotype_;
    };

    phenotype_pimpl_state phenotype_pimpl_state_;
    bool phenotype_pimpl_base_;
  };
}

#ifdef PHENOTYPE_PIMPL_HPP_CLEAR_OMIT_PAGGR
#  undef XSDE_OMIT_PAGGR
#endif

#ifndef XSDE_OMIT_PAGGR

#endif // XSDE_OMIT_PAGGR

// Begin epilogue.
//
//
// End epilogue.

#include <xsde/cxx/post.hxx>

#endif // PHENOTYPE_PIMPL_HPP
